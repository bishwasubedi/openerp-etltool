#!/usr/bin/env python

from __future__ import unicode_literals

#from abc import ABCMeta
from collections import OrderedDict
from ConfigParser import ConfigParser

import os
import sys
import csv

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../../pythonlib'))
import openobject as oo


# Constants.

ETL_DEBUG_LEVEL = 3
ETL_DEBUG_PRINT = 2

ETL_BATCH_SIZE = 100
ETL_WRITE_DATA = False

_PREPARED = 'Already prepared.'
_NOT_PREPARED = 'Not prepared.'

_UNIMPLEMENTED = 'Unimplemented abstract method.'
_CLASS_NOT_REG = 'Class: "%s" is not registered.'


# Interfaces.

class ETLInterface(object):
    def __init__(self, parent, config):
        raise NotImplementedError(_UNIMPLEMENTED)
    def cleanup(self):
        raise NotImplementedError(_UNIMPLEMENTED)
    def prepare(self):
        raise NotImplementedError(_UNIMPLEMENTED)

class ETLSource(ETLInterface):
    def lookup(self, obj, key='id', value='name', filter=''):
        raise NotImplementedError(_UNIMPLEMENTED)
    def read(self, size):
        raise NotImplementedError(_UNIMPLEMENTED)

class ETLTarget(ETLInterface):
    def lookup(self, obj, key='name', value='id', filter=''):
        raise NotImplementedError(_UNIMPLEMENTED)
    def write(self, rows):
        raise NotImplementedError(_UNIMPLEMENTED)

class ETLProcess(ETLInterface):
    def process(self, row):
        raise NotImplementedError(_UNIMPLEMENTED)


# Errors.

class ETLError(Exception): pass

class EmptyError(ValueError): pass

class ETLConfigError(ETLError): pass
class ETLCodingError(ETLError): pass

class ETLEngineError(ETLError): pass
class ETLSourceError(ETLError): pass
class ETLTargetError(ETLError): pass
class TransformError(ETLError): pass


# Abstract/Meta Classes.

class ETLMeta(type):

    __registry = {}

    def __new__(cls, name, bases, attrs):
        if '__readonly__' in attrs:
            #print name, attrs['__readonly__']
            for attr, default in attrs['__readonly__'].iteritems():
                _attr = '_' + attr
                attrs[_attr] = default
                attrs[attr] = cls._property(_attr)
        return super(ETLMeta, cls).__new__(cls, name, bases, attrs)

    def __init__(cls, name, bases, attrs):
        super(ETLMeta, cls).__init__(name, bases, attrs)
        cls._reg_clsss(cls, name)

    @classmethod
    def _get_class(cls, name):
        try:
            return cls.__registry[name]
        except KeyError:
            raise ETLCodingError(_CLASS_NOT_REG % name)

    @classmethod
    def _reg_clsss(cls, klass, name=None):
        if name is None:
            name = klass.__name__
        cls.__registry[name] = klass

    @staticmethod
    def _property(attr):
        return property(lambda self: getattr(self, attr))


class ETLBase(object):

    __metaclass__ = ETLMeta

    def __init__(self):
        raise NotImplementedError(_UNIMPLEMENTED)

    @staticmethod
    def _ct(var):
        if isinstance(var, basestring):
            if not var.startswith('+'):
                try:
                    if '.' not in var:
                        try:
                            return int(var)
                        except ValueError:
                            try:
                                return long(var)
                            except ValueError:
                                pass
                    return float(var)
                except ValueError:
                    pass
                val = var.lower()
                if val in ['true', 'yes']:
                    return True
                if val in ['false', 'no']:
                    return False
        return var

    @staticmethod
    def _uq(var):
        if var.startswith('"'):
            return var.strip('"')
        elif var.startswith("'"):
            return var.strip("'")
        else:
            return var

    @staticmethod
    def _dp(data, mul=72):
        if ETL_DEBUG_LEVEL >= ETL_DEBUG_PRINT:
            if isinstance(data, list):
                print '=' * mul
                for item in data:
                    if isinstance(item, dict):
                        for i in item.iteritems():
                            try:
                                print "%s: %s" % i
                            except (UnicodeDecodeError, UnicodeEncodeError):
                                pass
                        print '-' * mul
                    elif isinstance(item, list):
                        for i in enumerate(item):
                            try:
                                print "%s: %s" % i
                            except (UnicodeDecodeError, UnicodeEncodeError):
                                pass
                        print '-' * mul
                    else:
                        try:
                            print item
                        except (UnicodeDecodeError, UnicodeEncodeError):
                            pass
            elif isinstance(data, dict):
                for i in data.iteritems():
                    try:
                        print "%s: %s" % i
                    except (UnicodeDecodeError, UnicodeEncodeError):
                        pass
            else:
                try:
                    print data
                except (UnicodeDecodeError, UnicodeEncodeError):
                    pass


class ETLObject(ETLBase, ETLInterface):

    __readonly__ = {
                    'parent':   None,
                    'prepared': None,
                    }

    _config = None

    def __init__(self, config, parent=None):
        self._config = config
        self._parent = parent
        self._prepared = False

    def cleanup(self):
        self._prepared = False

    @property
    def config(self):
        return self._config.copy()

    def prepare(self):
        if self._prepared:
            raise ETLCodingError(_PREPARED)
        self._prepared = True


class Transform(ETLObject, ETLProcess):

    __readonly__ = {
                    'fields':   None,
                    'ordered':  None,
                    'firstrow': None,
                    }

    def cleanup(self):
        self._ordered = None
        self._firstrow = None
        self._fields = None
        super(Transform, self).cleanup()

    def prepare(self):
        super(Transform, self).prepare()
        self._fields = False
        self._firstrow = True
        self._ordered = self._parent.source.ordered and self._parent.target.ordered

    def process(self, row):
        raise NotImplementedError(_UNIMPLEMENTED)


class TLookupMap(Transform):

    _NO_PARENT = 'Unxepected "None" for parent.'

    def __init__(self, config, parent=None):
        if parent is None:
            raise ETLCodingError(self._NO_PARENT)
        super(TLookupMap, self).__init__(config, parent)


class TFunction(Transform):

    _funcs = None

    def __init__(self, config, parent=None):
        if self._funcs is None:
            raise ETLCodingError(_UNIMPLEMENTED)
        super(TFunction, self).__init__(config, parent)

    def process(self, row):

        if not self._prepared:
            raise ETLCodingError(_NOT_PREPARED)

        if self._firstrow:
            self._init(row)

        if self._fields:
            for field, func in self._fields.iteritems():
                row[field] = func(row[field])
        return row

    def _init(self, row):
        config = self._config
        funcs, fields = self._funcs, {}
        for field in config:
            if field in row:
                func = config.get_word(field)
                if func in funcs:
                    fields[field] = funcs[func]
        self._fields = fields
        self._firstrow = False



# Classes.

class ETLHooks(object):

    _hooks = None

    _UNDEF = 'Hook: "%s" is not defined.'

    def __init__(self, hooks=None):
        if hooks is None:
            hooks = {}
        elif isinstance(hooks, (list, tuple)):
            hooks = dict((hook, None) for hook in hooks)
        self.__dict__['_hooks'] = hooks

    def __getattr__(self, attr):
        try:
            return self._hooks[attr]
        except KeyError:
            raise AttributeError(self._UNDEF % name)

    def __setattr__(self, attr, value):
        if attr in self._hooks:
            self._hooks[attr] = value
        else:
            super(ETLHooks, self).__setattr__(attr, value)

    def get(self, name):
        return self._hooks.get(name, None)

    def set(self, name, hook):
        self._hooks[name] = hook


class ETLConfig(OrderedDict):

    _vartypes = {
                'double':   float,
                'float':    float,
                'int':      int,
                'long':     long,
                'single':   float,
                'str':      str,
                'string':   str,
                'unicode':  unicode,
                }

    _boolvals = {
                '1':        True,
                '0':        False,
                'true':     True,
                'false':    False,
                'yes':      True,
                'no':       False,
                'on':       True,
                'off':      False,
                }

    _errmsgs = {
                'undef': 'Option "%s" is undefined in the config.',
                'empty': 'Empty value provided for "%s" in config.',
                'etype': 'Value for the "%s" is of wrong type.',
                'econv': 'Unable to convert the type of "%s".',
                }

    _UNDEFINED = 'Option "%s" is not defined in config.'
    _NOVARTYPE = "Don't know how to convert '%s' to %s."

    _NO_SECTION = 'Section "%s" is not found in config.'
    _NOT_A_DICT = 'Expected section, but %s is a value.'

    def __delattr__(self, name):
        try:
            del self[name]
        except KeyError:
            super(ETLConfig, self).__delattr__(name)

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(self._UNDEFINED % name)

    def __setattr__(self, name, value):
        if name in self:
            self[name] = value
        else:
            super(ETLConfig, self).__setattr__(name, value)

    def get_var(self, name, default=None, vartype=None, errmsg=None, **kwds):

        try:

            value = self[name]

            if value == '':
                raise EmptyError

            if vartype is None:
                return value

            if vartype in self._vartypes:
                return self._vartypes[vartype](value)

            if vartype == 'bool':
                val = value.lower()
                if val in self._boolvals:
                    return self._boolvals[val]
                return bool(val)

            if vartype == 'num':
                if '.' not in value:
                    try:
                        return int(value)
                    except ValueError:
                        try:
                            return long(value)
                        except ValueError:
                            pass
                return float(value)

            if vartype in ['text', 'word']:
                if value.startswith('"'):
                    value = value.strip('"')
                elif value.startswith("'"):
                    value = value.strip("'")
                if vartype == 'word':
                    return value.split(None, 1)[0]
                return value

            if vartype in ['list', 'tuple']:
                sep = kwds['sep'] if 'sep' in kwds else ','
                value = value.split(sep)
                if vartype == 'tuple':
                    return tuple(value)
                return value

            if vartype == 'dict':
                if isinstance(value, dict):
                    return value
                raise TypeError(self._NOT_A_DICT % name)

        except EmptyError:
            error, args = 'empty', False
        except ValueError as e:
            error, args = 'econv', e.args
        except TypeError as e:
            error, args = 'etype', e.args
        except KeyError:
            error, args = 'undef', False
        else:
            error, args = False, False

        if error:
            if default is not None:
                return default
            if error in kwds:
                errmsg = kwds[error]
            elif errmsg is None:
                if vartype == 'dict':
                    errmsg = self._NO_SECTION % name
                else:
                    errmsg = self._errmsgs[error] % name
            if args:
                raise ETLConfigError(errmsg, *args)
            raise ETLConfigError(errmsg)

        raise ETLCodingError(self._NOVARTYPE % (vartype, name))

    get_bool    = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'bool', e, **k)
    get_dict    = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'dict', e, **k)
    get_float   = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'float', e, **k)
    get_int     = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'int', e, **k)
    get_list    = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'list', e, **k)
    get_long    = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'long', e, **k)
    get_num     = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'num', e, **k)
    get_str     = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'str', e, **k)
    get_text    = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'text', e, **k)
    get_tuple   = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'tuple', e, **k)
    get_word    = lambda s, n, d=None, e=None, **k: s.get_var(n, d, 'word', e, **k)

    def set(self, name, value):
        self[name] = value


class ETLParser(ConfigParser):

    def parse(self, filename):
        self.read(filename)
        sections = self._sections.copy()
        for section in sections:
            sections[section].pop('__name__', None)
        return sections


class ETLLogger(ETLBase):

    OFF     = 0
    ERROR   = 1
    WARN    = 2
    INFO    = 3
    DEBUG   = 4

    _status = {
                OFF:    'LOG',
    			ERROR:  'ERROR',
    			WARN:	'WARNING',
    			INFO:   'INFO',
    			DEBUG:  'DEBUG',
    			}

    __readonly__ = {
                    'filename': None,
                    'loglevel': None,
                    }

    _cache = {}

    _logdir = None
    _instances = {}

    def __init__(self, filename, loglevel=None):
        self._filename = filename
        self._loglevel = loglevel

    @classmethod
    def getInstance(cls, name, loglevel=None, prefix='', suffix='.log'):
        if name in cls._instances:
            return cls._instances[name]
        if cls._logdir is None:
            cls._logdir = os.path.abspath(os.path.dirname(__file__) + '/../log')
        filename = os.path.join(cls._logdir, prefix, name + suffix)
        instance = cls(filename, loglevel)
        cls._instances[name] = instance
        return instance

    def log(self, message, level=None, *args):
        pass

    def debug(self, message, *args):
        return self.log(message, self.DEBUG, *args)

    def error(self, message, *args):
        return self.log(message, self.ERROR, *args)

    def info(self, message, *args):
        return self.log(message, self.INFO, *args)

    def warn(self, message, *args):
        return self.log(message, self.WARN, *args)


class ETLRunner(ETLBase):

    __readonly__ = {
                    'maps':     None,
                    'hooks':    None,
                    'logdir':   None,
                    'logger':   None,
                    'config':   None,
                    }

    _check = None

    def __init__(self, names=None, config=None):
        self._maps = OrderedDict()
        self._config = config
        if names is not None:
            for name in names:
                self.add(name)
        self._logdir = ''
        self._logger = None
        self._hooks = ETLHooks(['created', 'prepare', 'cleanup'])

    def add(self, name, replace=True, config=None):
        if replace or name not in self.maps:
            map_ = self._parse(name)
            self._maps[name] = self._merge(name, map_, config)
            return True
        return False

    def get(self, name, existing=True, config=None):
        if existing and name in self._maps:
            map_ = self._maps[name]
        else:
            map_ = self._parse(name)
        return ETLEngine(self._merge(name, map_, config), self)

    def log(self, name, level=None):
        return ETLLogger.getInstance(name, level, self._logdir)

    def run(self, w=None, b=None):

        hooks = self._hooks

        created = hooks.created
        prepare = hooks.prepare
        cleanup = hooks.cleanup

        results = OrderedDict()

        for name, map_ in self._maps.iteritems():

            engine = ETLEngine(map_, self)

            if created:
                r = created('created', engine)
                if r is None:
                    break
                if r == False:
                    continue

            if prepare or cleanup:
                r = True
                engine.prepare()
                if prepare:
                    r = prepare('prepare', engine)
                if r:
                    result = engine.run(w, b)
                else:
                    result = r
                if r and cleanup:
                    r = cleanup('cleanup', engine)
                engine.cleanup()
                if r is None:
                    break
            else:
                result = engine.run(w, b)

            results[name] = result

        return results

    def _merge(self, name, map_, config_=None):
        if self._config:
            config = self._config.copy()
        else:
            config = None
        if config_:
            if config:
                config.update(config_)
            else:
                config = config_.copy()
        if config:
            config.update(map_)
        else:
            config = map_
        if 'engine' in config:
            config['engine']['name'] = name
        return ETLConfig((k, ETLConfig(v)) for k, v in config.iteritems())

    def _parse(self, name):

        if '.' not in name:
            name += '.ini'
        fn = 'etc/' + name

        if self._check is None:
            self._check = []
            check = True
        else:
            check = False

        if fn in self._check:
            return None

        self._check.append(fn)

        p = ETLParser()
        ini = p.parse(fn)
        cfg = OrderedDict()

        if 'includes' in ini:
            for include in ini['includes'].itervalues():
                inc = self._parse(include)
                if inc is not None:
                    cfg.update(inc)

        for sec, opts in ini.iteritems():
            if ':' in sec:
                sec, base = sec.split(':', 1)
                if base in cfg:
                    if sec in cfg:
                        cfg[sec].update(cfg[base])
                    else:
                        cfg[sec] = cfg[base].copy()
            if sec in cfg:
                cfg[sec].update(opts)
            else:
                cfg[sec] = opts

        if check:
            self._check = None

        return cfg


class ETLEngine(ETLObject):

    BATCH_SIZE = ETL_BATCH_SIZE
    WRITE_DATA = ETL_WRITE_DATA

    batchsize = None
    writedata = None

    __readonly__ = {
                    'name':     None,
                    'hooks':    None,
                    'logger':   None,
                    'source':   None,
                    'target':   None,
                    }

    _procs = None

    _NO_CLASS = 'Class: "%s" not found.'

    _NOENGINE = 'Engine settings not found.'
    _NOSECTON = 'Settings for "%s" not found.'

    def __init__(self, config, parent=None):

        super(ETLEngine, self).__init__(config, parent)

        engine = config.get_dict('engine', None, self._NOENGINE)
        self._name = name = engine.get_word('name', 'etl_engine')

        self._logger = parent.log(name) if parent else ETLLogger.getInstance(name)

        self.writedata = engine.get_bool('writedata', self.WRITE_DATA)
        self.batchsize = engine.get_int('batchsize', self.BATCH_SIZE)

        self._source = self._cls(engine.get_word('source'), 'source')
        self._target = self._cls(engine.get_word('target'), 'target')

        procs = OrderedDict()
        process = config.get_dict('process', False)
        if process:
            for proc in process.iterkeys():
                cls = process.get_word(proc)
                procs[proc] = self._cls(cls, proc)
        self._procs = procs

        self._hooks = ETLHooks(['postread', 'pretrans', 'postproc', 'prewrite', 'writeres'])

    def cleanup(self):
        for proc in reversed(self._procs.values()):
            proc.cleanup()
        self._target.cleanup()
        self._source.cleanup()
        super(ETLEngine, self).cleanup()

    def prepare(self):
        super(ETLEngine, self).prepare()
        self._source.prepare()
        self._target.prepare()
        for proc in self._procs.values():
            proc.prepare()

    def run(self, writedata=None, batchsize=None):

        if not self._prepared:
            self.prepare()
            cleanup = True
        else:
            cleanup = False

        source = self._source
        target = self._target

        if writedata is None:
            writedata = self.writedata
        else:
            writedata = bool(writedata)

        if batchsize is None:
            batchsize = self.batchsize
        else:
            batchsize = int(batchsize)

        hooks, procs = self._hooks, self._procs

        postread = hooks.postread
        pretrans = hooks.pretrans
        postproc = hooks.postproc
        prewrite = hooks.prewrite
        writeres = hooks.writeres

        result = []

        while True:

            # Read rows from source.
            rows = source.read(batchsize)
            if not rows:
                break

            numrows = len(rows)

            # Call post-read hook if set.
            if postread:
                runok = postread('postread', rows)
                if runok is None:
                    break
            else:
                runok = True

            if runok:

                # Transform each row and/or run
                # pretrans/postproc hooks if set.
                if procs or pretrans or postproc:
                    _rows, indexes = [], []
                    for idx, row in enumerate(rows):
                        # Call pre-trans hook.
                        if pretrans:
                            runok = pretrans('pretrans', row)
                            if runok is None:
                                break
                        else:
                            runok = True
                        if runok and procs:
                            # Run transformations.
                            for name, proc in procs.iteritems():
                                row = proc.process(row)
                                if not row:
                                    continue
                            # Call post-proc hook.
                            if postproc:
                                runok = postproc('postproc', row)
                                if runok is None:
                                    break
                                elif runok == False:
                                    continue
                        _rows.append(row)
                        indexes.append(idx)
                    # End for idx, row in enumerate(rows): Process rows.
                    if _rows:
                        runok = True
                        if len(_rows) == numrows:
                            indexes = False
                        rows = _rows
                    else:
                        runok = False
                        rows = []
                else:
                    indexes = False
                # End if procs or pretrans or postproc: Transform rows.

                if runok:

                    # Call pre-write hook if set.
                    if prewrite:
                        runok = prewrite('prewrite', rows)
                        if runok is None:
                            break

                    if runok:

                        if ETL_DEBUG_LEVEL >= ETL_DEBUG_PRINT:
                            self._dp(rows)

                        # Write rows to target.
                        if writedata:
                            res = target.write(rows)
                            # Call write-result hook if set.
                            if writeres:
                                runok = writeres('writeres', res)
                            # Process result.
                            if isinstance(res, dict):
                                if res['ids'] != False:
                                    ids = res['ids']
                                else:
                                    ids = [False] * len(rows)
                            else:
                                ids = res
                            if indexes:
                                lst = [None] * numrows
                                for i, id in enumerate(ids):
                                    lst[indexes[i]] = id
                                result += lst
                            else:
                                result += ids
                            if runok is None:
                                break
                            continue
                        # End if writedata:

                    # End if runok: prewrite hook.

                # End if runok: process/transform.

            # End if runok: postread hook.

            result += [None] * numrows

        # End while True:

        if cleanup:
            self.cleanup()

        return result

    def _cls(self, clsname, cfgname):
        try:
            cls = self.__cls(clsname)
        except ETLCodingError as e:
            raise ETLConfigError(self._NO_CLASS % clsname, *e.args)
        config = self._config.get_dict(cfgname, None, self._NOSECTON % cfgname)
        return cls(config, self)

    @classmethod
    def __cls(cls, name):
        return cls._get_class(name)


# Transforms.

class TConverter(TFunction):

    _funcs = {
                'boolean':  lambda x: bool(x),
                'float':    lambda x: float(x),
                'integer':  lambda x: int(x),
                'null':     lambda x: None,
                'string':   lambda x: str(x),
                }


class TFieldsMap(Transform):

    __readonly__ = {'values': None}

    def process(self, row):

        if not self._prepared:
            raise TransformError(_NOT_PREPARED)

        if self._firstrow:
            self._init(row)

        if self._fields:
            values = self._values
            r = OrderedDict() if self._ordered else {}
            for field, action in self._fields.iteritems():
                if isinstance(action, basestring):
                    if field in values:
                        value = values[field]
                    else:
                        value = row[action]
                    r[field] = value
                else:
                    value = values[field].join(row[i] for i in action)
                    r[field] = value
            return r
        return row

    def _init(self, row):

        ordered = self._ordered

        fields = OrderedDict((key, key) for key in row.iterkeys())

        if ordered:
            sorder = OrderedDict((key, idx) for idx, key in enumerate(fields.iterkeys()))
            torder, dorder, order = OrderedDict(), [], 0

        config, values = self._config, {}

        for field in config:
            action = config.get_text(field)
            if action == '-':
                if ordered:
                    dorder.append(field)
                del fields[field]
            elif ',' in action:
                if ':' in action:
                    value = action.split(':')
                    parts = value[0].split(',')
                    values[field] = value[1]
                    if len(value) > 2 and value[2] == '-':
                        for part in parts:
                            if ordered:
                                dorder.append(part)
                            del fields[part]
                else:
                    parts = action.split(',')
                    values[field] = ' '
                if ordered:
                    torder[field] = sorder[parts[0]]
                    order = sorder[parts[1]]
                fields[field] = parts
            elif action.startswith('+'):
                if ordered and order:
                    torder[field] = order
                    order = 0
                value = self._ct(action[1:])
                if isinstance(value, basestring):
                    values[field] = self._uq(value)
                else:
                    values[field] = value
                fields[field] = field
            elif action.startswith('-'):
                action = action[1:]
                if ordered:
                    torder[field] = sorder[action]
                    dorder.append(action)
                    order = 0
                del fields[action]
                fields[field] = action
            else:
                if ordered and order:
                    torder[field] = order
                    order = 0
                fields[field] = action

        if ordered:
            forder = dict((k, v) for k, v in sorder.iteritems() if k not in dorder)
            forder.update(torder)
            self._fields = OrderedDict((k, fields[k]) for k in sorted(forder.keys(), key=lambda x: forder[x]))
        else:
            self._fields = fields

        self._values = values
        self._firstrow = False


class TNullValue(Transform):

    def process(self, row):

        if not self._prepared:
            raise TransformError(_NOT_PREPARED)

        if self._firstrow:
            self._init(row)

        if self._fields:
            for field, value in self._fields.iteritems():
                if row[field] is None:
                    row[field] = value
        return row

    def _init(self, row):
        config = self._config
        if '*' in config:
            value = config.get('*')
            fields = dict((key, value) for key in row.iterkeys())
        else:
            fields = {}
        for field, value in config.iteritems():
            if field in row:
                fields[field] = value
        self._fields = fields
        self._firstrow = False


class TTrimValue(TFunction):

    _funcs = {
                'trim':     lambda s: s.strip(),
                'ltrim':    lambda s: s.lstrip(),
                'rtrim':    lambda s: s.rstrip(),
                }



class TSingleMap(TLookupMap):

    __readonly__ = {'lookup': None}

    def cleanup(self):
        self._fields = None
        self._lookup = None
        super(TSingleMap, self).cleanup()

    def prepare(self):

        super(TSingleMap, self).prepare()

        config = self._config
        fields, lookup = {}, {}

        for key in config:

            val = config.get_text(key)

            parts = val.split(':', 2)
            field = parts[0] or key

            name = parts[1] or 'target'
            func = getattr(self._parent, name).lookup
            args = parts[2].split(',', 3)
            
            lookup[key] = func(*args)
            fields[key] = field

        self._lookup = lookup
        self._fields = fields

    def process(self, row):
        if not self._prepared:
            raise TransformError(_NOT_PREPARED)
        lookup = self._lookup
        for key, field in self._fields.iteritems():
            value = row[field]
            try:
                row[key] = lookup[key][value]
            except KeyError:
                if key != field and key not in row:
                    row[key] = value
        return row


class TDoubleMap(TLookupMap):

    __readonly__ = {
                    'slookup': None,
                    'tlookup': None,
                    }

    def cleanup(self):
        self._fields = None
        self._tlookup = None
        self._slookup = None
        super(TDoubleMap, self).cleanup()

    def prepare(self, engine):

        super(TDoubleMap, self).prepare(engine)

        config = self._config
        fields, slookup, tlookup = {}, {}, {}

        for key in config:

            val = config.get_text(key)

            parts = val.split(':', 2)
            field = parts[0] or key

            sargs = parts[1].split(',', 3)
            targs = parts[2].split(',', 3)

            slookup[key] = self._parent.source.lookup(*sargs)
            tlookup[key] = self._parent.target.lookup(*targs)

            fields[key] = field

        self._slookup = slookup
        self._tlookup = tlookup

        self._fields = fields

    def process(self, row):

        if not self._prepared:
            raise TransformError(_NOT_PREPARED)

        slookup, tlookup = self._slookup, self._tlookup

        def lookup(id):
            try:
                name = slookup[key][id]
                return tlookup[key][name]
            except KeyError:
                return id

        for key, field in self._fields.iteritems():
            value = row[field]
            if value:
                if ',' in value:
                    row[key] = ','.join([lookup(id) for id in value.split(',')])
                else:
                    row[key] = lookup(value)
            elif key != field and key not in row:
                row[key] = value

        return row



# CSV Source and Target.

class CSVObject(ETLObject):

    READ = 'rb'
    WRITE = 'wb'

    __readonly__ = {
                    'filename': None,
                    'fields':   None,
                    'header':   None,
                    'ordered':  None,
                    }

    _csv = None

    _file = None
    _mode = None

    _order = None

    _INVALID_MODE = 'Invalid mode: %s'
    _MODE_NOT_SET = 'Mode not specified'

    _NO_FILENAME = 'Filename not set in config.'

    def __init__(self, config, parent=None):

        super(CSVObject, self).__init__(config, parent)

        try:
            mode = config.pop('mode')
        except KeyError:
            raise ETLCodingError(self._MODE_NOT_SET)

        if mode not in [self.READ, self.WRITE]:
            raise ETLCodingError(self._INVALID_MODE % mode)
        self._mode = mode

        self._filename = config.get_text('filename', None, self._NO_FILENAME)

        self._fields = config.get_list('fields', False)
        self._ordered = config.get_bool('ordered', True)

    def cleanup(self):
        self._csv = None
        self._file.close()
        self._file = None
        self._order = None
        self._header = None
        super(CSVObject, self).cleanup()

    def lookup(self, obj, key, value, filter=''):

        if not self._prepared:
            raise ETLCodingError(_NOT_PREPARED)

        if filter:
            raise ETLConfigError('Filter is not implemented.')

        r = {}

        with open(obj, 'rb') as f:
            reader = csv.reader(f)
            fields = reader.next()
            id = fields.index(key)
            name = fields.index(value)
            for row in reader:
                if row == []:
                    continue
                r[row[id]] = row[name]

        return r

    def prepare(self):
        super(CSVObject, self).prepare()
        mode = self._mode
        if mode == self.READ:
            self._file = open(self._filename, mode)
            self._csv = csv.reader(self._file)
        elif mode == self.WRITE:
            self._file = open(self._filename, mode)
            self._csv = csv.writer(self._file)
        else:
            raise ETLCodingError(self._INVALID_MODE % mode)

    def _read(self):
        data = []
        try:
            while data == []:
                data = self._csv.next()
        except StopIteration:
            pass
        return data

    def _write(self, data):
        return self._csv.writerow(data)


class CSVSource(CSVObject, ETLSource):

    def __init__(self, config, parent=None):
        config['mode'] = self.READ
        super(CSVSource, self).__init__(config, parent)

    def lookup(self, obj, key='id', value='name', filter=''):
        return super(CSVSource, self).lookup(obj, key, value, filter)

    def read(self, size):

        if not self._prepared:
            raise ETLCodingError(_NOT_PREPARED)

        fields, header = self._fields, self._header
        ordered, order = self._ordered, self._order

        dc = OrderedDict if ordered else dict

        if header is None:
            header = self._read()
            if not header:
                raise ETLSourceError('Cannot read CSV header.')
            if fields:
                order = dc((f, i) for i, f in enumerate(header) if f in fields)
                self._order = order
            self._header = header

        rows = []
        index = 0

        while True:
            data = self._read()
            if not data:
                break
            if fields:
                row = dc((f, data[i]) for f, i in order.iteritems())
            else:
                row = dc(zip(header, data))
            rows.append(row)
            index += 1
            if size > 0 and index >= size:
                break

        return rows


class CSVTarget(CSVObject, ETLTarget):

    dryrun = None

    def __init__(self, config, parent=None):
        config['mode'] = self.WRITE
        super(CSVTarget, self).__init__(config, parent)
        self.dryrun = config.get_bool('dryrun', False)

    def lookup(self, obj, key='name', value='id', filter=''):
        return super(CSVTarget, self).lookup(obj, key, value, filter)

    def write(self, rows):

        if not self._prepared:
            raise ETLCodingError(_NOT_PREPARED)

        dryrun = self.dryrun

        fields, header = self._fields, self._header
        ordered, order = self._ordered, self._order

        dc = OrderedDict if ordered else dict

        if header is None:
            header = rows[0].keys()
            if fields:
                order = [f for f in fields if f in header]
                self._write(order)
                self._order = order
            else:
                self._write(header)
            self._header = header

        for row in rows:
            if fields:
                data = [row[f] for f in order]
            else:
                data = [row[f] for f in header]
            if dryrun:
                continue
            self._write(data)

        return [True] * len(rows)



# OpenERP Source and Target.

class OERPObject(ETLObject):

    __readonly__ = {
                    'oorpc':        None,
                    'model':        None,
                    'object':       None,
                    'fields':       None,
                    'ftypes':       None,
                    'ordered':      False,
                    'database':     None,
                    'rel_fields':   None,
                    'rel_ftypes':   None,
                    'many2one':     None,
                    'one2many':     None,
                    }

    _firstrun = None

    _NO_LOGIN = 'Unable to login: %s.'

    __m2o = {'id': 0, 'name': 1}
    __o2m = {'add': [',', False], 'set': [',', True]}

    def __init__(self, config, parent=None):

        super(OERPObject, self).__init__(config, parent)

        self._model = config.get_word('model')
        self._database = config.get_word('database')

        location = config.get_text('location', '')
        if location:
            self._oorpc = oo.OpenObjectRpc(location)
        else:
            self._oorpc = oo.OpenObjectRpc()

        fields, ftypes = [], {}

        fields_str = config.get_text('fields', '')
        if fields_str:
            all_fields = fields_str.startswith(':')
            if all_fields:
                fields_str = fields_str[1:]
            for field in fields_str.split(','):
                if '.' in field:
                    field, ftype = field.split('.')
                    ftypes[field] = ftype
                if not all_fields:
                    fields.append(field)

        self._fields, self._ftypes = fields, ftypes

    def cleanup(self):
        self._object = self._mod_fields = None
        self._rel_ftypes = None
        self._rel_fields = None
        self._firstrun = None
        super(OERPObject, self).cleanup()

    def lookup(self, obj, key, value, filter=''):

        if not self._prepared:
            raise ETLCodingError(_NOT_PREPARED)

        if '.' in key:
            key, k = key.split('.', 1)
        else:
            k = False

        if '.' in value:
            value, v = value.split('.', 1)
        else:
            v = False

        search = self._parse(filter) if filter else []
        rows = self._object.readall(obj, search, [key, value])

        if k and v:
            k = 1 if k == 'name' else 0
            v = 1 if v == 'name' else 0
            return dict((row[key][k], row[value][v]) for row in rows)
        elif k:
            k = 1 if k == 'name' else 0
            return dict((row[key][k], row[value]) for row in rows)
        elif v:
            v = 1 if v == 'name' else 0
            return dict((row[key], row[value][v]) for row in rows)
        else:
            return dict((row[key], row[value]) for row in rows)

    def prepare(self):
        super(OERPObject, self).prepare()
        username = self._config.get_text('username', 'admin')
        password = self._config.get_text('password', 'admin')
        id = self._oorpc.login(self._database, username, password)
        if not id:
            raise ETLError(self._NO_LOGIN % self._database)
        self._object = self._oorpc.object
        self._rel_fields = False
        self._rel_ftypes = False
        self._firstrun = True

    def _init(self, row, _m2o, _o2m):

        __m2o, __o2m = self.__m2o, self.__o2m

        many2one, one2many = self._many2one, self._one2many

        fields = self._object.fields(self._model, self._fields)
        ftypes = self._ftypes

        rel_fields = {}

        if row and not self._fields:
            for name, field in fields.iteritems():
                reltype = field['type']
                if '2' in reltype:
                    if (name in row
                        or (reltype == 'many2one'
                            and name+'_id' in row
                            and name+'_name' in row)):
                        rel_fields[name] = reltype
        else:
            for name, field in fields.iteritems():
                reltype = field['type']
                if '2' in reltype:
                    rel_fields[name] = reltype

        m2o, o2m = {}, {}

        for name, rtype in rel_fields.iteritems():
            if name in ftypes:
                ftype = ftypes[name]
                if rtype == 'many2one':
                    if ftype not in _m2o:
                        ftype = many2one
                else:
                    if ftype not in _o2m:
                        ftype = one2many
            else:
                ftype = many2one if rtype == 'many2one' else one2many
            if row == False:
                if ftype in __m2o:
                    m2o[name] = __m2o[ftype]
                elif ftype == 'both':
                    m2o[name] = [name+'_id', name+'_name']
                elif ftype == 'join':
                    o2m[name] = ','
            else:
                if ftype in __o2m:
                    o2m[name] = __o2m[ftype]
                elif ftype == 'id':
                    m2o[name] = [0, name+'_id', name+'_name']
                elif ftype == 'name':
                    m2o[name] = [1, name+'_name', name+'_id']

        rel_ftypes = {}

        if m2o:
            rel_ftypes['m2o'] = m2o
        if o2m:
            rel_ftypes['o2m'] = o2m

        self._rel_fields = rel_fields
        self._rel_ftypes = rel_ftypes

        self._firstrun = False

    def _init_rel(self, _m2o, _o2m, m2o, o2m):
        many2one = self._config.get_word('many2one', m2o)
        one2many = self._config.get_word('one2many', o2m)
        self._many2one = many2one if many2one in _m2o else m2o
        self._one2many = one2many if one2many in _o2m else o2m

    def _parse(self, search, ts='+', fs=','):
        if isinstance(search, list):
            return search
        def tpl(parts):
            index = len(parts) - 1
            parts[index] = self._ct(parts[index])
            return tuple(parts)
        # Short Version:
        return [tpl(item.split(fs)) if fs in item else item for item in search.split(ts)]
        # Long Version:
        domain = []
        for item in search.split(ts):
            if fs in item:
                domain.append(tpl(item.split(fs)))
            else:
                domain.append(item)
        return domain


class OERPSource(OERPObject, ETLSource):

    MANY2ONE = 'both'
    ONE2MANY = 'join'

    __readonly__ = {
                    'offset': None,
                    'search': None,
                    }

    __m2o = ['raw', 'id', 'name', 'both']
    __o2m = ['raw', 'join']

    def __init__(self, config, parent=None):
        super(OERPSource, self).__init__(config, parent)
        search = config.get_text('search', '')
        self._search = self._parse(search) if search else []
        self._init_rel(self.__m2o, self.__o2m, self.MANY2ONE, self.ONE2MANY)

    def cleanup(self):
        self._offset = None
        super(OERPSource, self).cleanup()

    def lookup(self, obj, key='id', value='name', filter=''):
        return super(OERPSource, self).lookup(obj, key, value, filter)

    def prepare(self):
        super(OERPSource, self).prepare()
        self._offset = 0

    def read(self, size):

        if not self._prepared:
            raise ETLCodingError(_NOT_PREPARED)

        if self._firstrun:
            self._init(False, self.__m2o, self.__o2m)

        limit = size if size >= 0 else None
        rows = self._object.page(self._model, self._search, self._fields, self._offset, limit)
        self._offset += len(rows)

        if len(self._rel_ftypes) > 0:
            rel_ftypes = self._rel_ftypes
            m2o = rel_ftypes.get('m2o', False)
            o2m = rel_ftypes.get('o2m', False)
            for row in rows:
                if m2o:
                    for name, field in m2o.iteritems():
                        if isinstance(field, (list, tuple)):
                            row[field[0]] = row[name][0]
                            row[field[1]] = row[name][1]
                            del row[name]
                        else:
                            row[name] = row[name][field]
                if o2m:
                    for name, field in o2m.iteritems():
                        row[name] = field.join(row[name])

        return rows


class OERPTarget(OERPObject, ETLTarget):

    MANY2ONE = 'raw'
    ONE2MANY = 'set'

    dryrun = None

    __readonly__ = {
                    'import': None,
                    'create': None,
                    'update': None,
                    'column': None,
                    'filter': None,
                    }

    _update_lookup = None
    _update_column = None

    __m2o = ['raw', 'id', 'name']
    __o2m = ['raw', 'add', 'set']

    def __init__(self, config, parent=None):
        super(OERPTarget, self).__init__(config, parent)
        self._import = config.get_bool('import', True)
        self._create = config.get_bool('create', True)
        self._update = config.get_bool('update', False)
        self._column = config.get_word('column', 'name')
        self._filter = config.get_text('filter', '')
        self.dryrun = config.get_bool('dryrun', False)
        self._init_rel(self.__m2o, self.__o2m, self.MANY2ONE, self.ONE2MANY)

    def cleanup(self):
        if self._update and not self._import:
            self._update_lookup = None
            self._update_column = None
        super(OERPTarget, self).cleanup()

    def lookup(self, obj, key='name', value='id', filter=''):
        return super(OERPTarget, self).lookup(obj, key, value, filter)

    def prepare(self):
        super(OERPTarget, self).prepare()
        if self._update and not self._import:
            column = self._column
            if '.' in column:
                self._update_column = column.split('.', 1)[0]
            else:
                self._update_column = column
            self._update_lookup = self.lookup(self._model, column, 'id', self._filter)

    def write(self, rows):

        if not self._prepared:
            raise ETLCodingError(_NOT_PREPARED)

        if self._firstrun:
            self._init(rows[0], self.__m2o, self.__o2m)

        if not self._import:
            return self._write(rows)

        fields = self._fields

        if fields:
            flds = fields
        else:
            flds = False
        data = []

        rel = len(self._ftypes) > 0

        for row in rows:
            if rel:
                self._rel(row)
            if fields:
                row = dict((f, row[f]) for f in fields)
            if not flds:
                flds = row.keys()
            values = row.values()
            data.append(map(str, values))
        if self.dryrun:
            res = {'ids': [True] * len(data), 'messages': ['dryrun']}
        else:
            res = self._object.load(self._model, flds, data)
        if ETL_DEBUG_LEVEL > ETL_DEBUG_PRINT:
            if res['ids'] != False:
                print 'Import Success * Result:', res['ids']
            else:
                print 'Import Failed! * Errors:', res['messages']
        return res

    def _write(self, rows):

        model, obj = self._model, self._object
        create, update = self._create, self._update

        if update:
            column = self._update_column
            lookup = self._update_lookup

        dryrun = self.dryrun

        fields, ids = self._fields, []

        rel = len(self._ftypes) > 0

        for row in rows:

            if rel:
                self._rel(row)

            if fields:
                row = dict((f, row[f]) for f in fields)

            if update and row[column] in lookup:
                id = lookup[row[column]]
                if dryrun:
                    res = id
                else:
                    res = obj.write(model, id, row)
                    if res:
                        res = id
                if ETL_DEBUG_LEVEL > ETL_DEBUG_PRINT:
                    print 'Update * Result:', res, '* Row:', row
                ids.append(res)
                continue

            if create:
                if dryrun:
                    res = True
                else:
                    res = obj.create(model, row)
                if ETL_DEBUG_LEVEL > ETL_DEBUG_PRINT:
                    print 'Create * Result:', res, '* Row:', row
                ids.append(res)

        return ids

    def _rel(self, row):
        rel_ftypes = self._rel_ftypes
        if 'm2o' in rel_ftypes:
            for name, field in rel_ftypes['m2o'].iteritems():
                if isinstance(row[name], (list, tuple)):
                    row[name] = row[name][0]
                else:
                    row[name] = row[field[1]]
                    del row[field[1]]
                    del row[field[2]]
        if 'o2m' in rel_ftypes:
            for name, field in rel_ftypes['o2m'].iteritems():
                vals = row[name]
                if not isinstance(vals, (list, tuple)):
                    vals = vals.split(field[0])
                if field[1]:
                    lst = [(6, 0, vals)]
                else:
                    lst = [(4, v) for v in vals]
                row[name] = lst



class TSMapArray(TSingleMap):

    _flags = None

    def process(self, row):

        if not self._prepared:
            raise TransformError(_NOT_PREPARED)

        if self._firstrow:
            self._flags = {}
            self._firstrow = False

        def num(var):
            try:
                int(var)
                return True
            except ValueError:
                return False

        flags, _lookup = self._flags, self._lookup

        for key, field in self._fields.iteritems():
            lookup = _lookup[key]
            value = row[field]
            if value:
                if key in flags:
                    flag = flags[key]
                elif isinstance(value, (list, tuple)):
                    if len(value) == 2 and num(value[0]) and not num(value[1]):
                        if key == field or isinstance(row[key], (list, tuple)):
                            flag = True
                        else:
                            flag = False
                    else:
                        flag = None
                    flags[key] = flag
                elif value in lookup:
                    row[key] = lookup[value]
                    continue
                if flag is None:
                    row[key] = [lookup[v] if v in lookup else v for v in value]
                elif value[1] in lookup:
                    if flag == True:
                        row[key][0] = lookup[value[1]]
                    else:
                        row[key] = lookup[value[1]]
                elif key != field:
                    row[key] = value if flag == True else value[0]
            elif key != field and key not in row:
                row[key] = value
        return row


class TDMapArray(TDoubleMap):

    _flags = None

    def process(self, row):

        if not self._prepared:
            raise TransformError(_NOT_PREPARED)

        if self._firstrow:
            self._flags = {}
            self._firstrow = False

        slookup, tlookup = self._slookup, self._tlookup

        def lookup(id):
            try:
                name = slookup[key][id]
                return tlookup[key][name]
            except KeyError:
                return id

        flags = self._flags

        for key, field in self._fields.iteritems():
            value = row[field]
            if value:
                if key in flags:
                    flag = flags[key]
                elif isinstance(value, (list, tuple)):
                    flags[key] = flag = False
                elif ',' in value:
                    flags[key] = flag = True
                else:
                    row[key] = lookup(value)
                    continue
                ids = value.split(',') if flag else value
                value = [lookup(id) for id in ids]
                row[key] = ','.join(value) if flag else value
            elif key != field and key not in row:
                row[key] = value

        return row



# Test Code.

if __name__ == '__main__':

    def dp(data, mul=72):
        if isinstance(data, list):
            print '=' * mul
            for item in data:
                if isinstance(item, dict):
                    for i in item.iteritems():
                        try:
                            print "%s: %s" % i
                        except (UnicodeDecodeError, UnicodeEncodeError):
                            pass
                    print '-' * mul
                elif isinstance(item, list):
                    for i in enumerate(item):
                        try:
                            print "%s: %s" % i
                        except (UnicodeDecodeError, UnicodeEncodeError):
                            pass
                    print '-' * mul
                else:
                    try:
                        print item
                    except (UnicodeDecodeError, UnicodeEncodeError):
                        pass
        elif isinstance(data, dict):
            for i in data.iteritems():
                try:
                    print "%s: %s" % i
                except (UnicodeDecodeError, UnicodeEncodeError):
                    pass
        else:
            try:
                print data
            except (UnicodeDecodeError, UnicodeEncodeError):
                pass

    config = {
                'location': 'http://localhost:4069/xmlrpc/2/',
                'database': 'bazaar_dev',
                'username': 'admin',
                'password': 'admin',
                'model':    'product.template',
                'fields':   'id,ean13,name,currency_id,currency_list_price',
                'search':   'ean13,!=,False',
                }

    source = OERPSource(ETLConfig(config))
    
    source.prepare()
    data = source.read(10)
    source.cleanup()
    
    dp(data)
    
    