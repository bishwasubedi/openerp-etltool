<?php


	require_once(dirname(__FILE__) . '/etlengine.php');


	abstract class CSVObject extends ETLObject {

		protected $csv = null;

		protected $filename = null;

		protected $ordered = null;

		protected $fields = null;
		protected $_fields = false;
		
		
		const _NO_IMPL = '%s: Filter is not implemented.';
		
		const _CLASS_FILE = '/../inc/csvfile.php';
		
    	const _NO_FILENAME = 'Filename not set in config.';
		
    	const _CLASS_NOT_SET = '%s: Class not specified.';
		const _INVALDI_CLASS = '%s: Class "%s" not exist.';
		
		
		public function __construct($config, $parent=null) {
			
			parent::__construct($config, $parent);
			
			try {
				$class = $config->pop('class');
			}
			catch (ETLConfigError $e) {
				throw new ETLCodingError(sprintf(self::_CLASS_NOT_SET, __METHOD__, $class));
			}
			
			if (!class_exists($class))
				require_once(dirname(__FILE__) . self::_CLASS_FILE);
			if (!class_exists($class))
				throw new ETLCodingError(sprintf(self::_INVALDI_CLASS, __METHOD__, $class));
			
			$this->csv = new $class();

			$this->filename = $config->get_text('filename', null, self::_NO_FILENAME);
			
			$this->fields = $config->get_list('fields', false);
			$this->ordered = $config->get_bool('ordered', true);

		}

		public function cleanup() {
			$this->csv->close();
			parent::cleanup();
		}

		public function lookup($object, $key, $value, $filter = '') {

			if ($filter)
				throw new ETLConfigError(sprintf(self::_NO_IMPL, __METHOD__));

			$csv = new CSVReader();

			if ($csv->open($object)) {
				$r = $csv->readcolumn($value, $key);
				$csv->close();
				return $r;
			}

			return false;

		}

		public function prepare() {
			parent::prepare();
			$this->csv->open($this->filename);
		}

	}


	class CSVSource extends CSVObject implements ETLSource {
		
		const _CSV_CLASS = 'CSVReader';
		
		public function __construct($config, $parent=null) {
			$config['class'] = self::_CSV_CLASS;
			parent::__construct($config, $parent);
		}
		
		public function lookup($object, $key = 'id', $value = 'name', $filter = '') {
			return parent::lookup($object, $key, $value, $filter);
		}
		
		public function read($size) {

			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));

			$rows = array();
			$csv = $this->csv;

			if (!$csv->eof) {

				if ($size > 0)
					$index = 0;

				$fields = $this->_fields;

				while ($row = $csv->read(true)) {

					if ($fields)
						$rows[] = array_intersect_key($row, $fields);
					else
						$rows[] = $row;

					if ($size > 0) {
						$index++;
						if ($index >= $size) {
							break;
						}
					}

				}

			}

			return $rows;

		}
		
	}


	class CSVTarget extends CSVObject implements ETLTarget {
		
		public $dryrun = null;
		
		const _CSV_CLASS = 'CSVWriter';

		public function __construct($config, $parent=null) {
			$config['class'] = self::_CSV_CLASS;
			parent::__construct($config, $parent);
			$this->dryrun = $config->get_bool('dryrun', false);
		}

		public function lookup($object, $key = 'name', $value = 'id', $filter = '') {
			return parent::lookup($object, $key, $value, $filter);
		}

		public function write($rows) {

			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));

			$csv = $this->csv;
			
			$dryrun = $this->dryrun;
			$fields = $this->_fields;

			$ids = array();
			foreach ($rows as $row) {
				if ($fields)
					$row = array_intersect_key($row, $fields);
				if ($dryrun) {
					$ids[] = true;
					continue;
				}
				$ids[] = $csv->write($row, true) ? true : false;
			}
			return $ids;

		}

	}

	

?>