<?php

	define('ETL_DEBUG_LEVEL',	2);
	define('ETL_DEBUG_PRINT',	2);

	define('ETL_BATCH_SIZE',	100);
	define('ETL_WRITE_DATA',	false);


	if (ETL_DEBUG_LEVEL > 0)
		define('ETL_LOG_LEVEL',	ETLLogger::DEBUG);
	else
		define('ETL_LOG_LEVEL',	ETLLogger::INFO);

	define('DS', DIRECTORY_SEPARATOR);


	// Interfaces.

	interface ETLInterface {
		public function __construct($config, $parent=null);
		public function cleanup();
		public function prepare();
	}

	interface ETLSource extends ETLInterface {
		public function lookup($object, $key='id', $value='name', $filter='');
		public function read($size);
	}

	interface ETLTarget extends ETLInterface {
		public function lookup($object, $key='name', $value='id', $filter='');
		public function write($rows);
	}

	interface ETLProcess extends ETLInterface {
		public function process($row);
	}


	// Errors.
	
	class ETLError extends Exception {}
	
	class ETLNoImplError extends ETLError {}
	
	class ETLConfigError extends ETLError {}
	class ETLCodingError extends ETLError {}
	
	class ETLRunnerError extends ETLError {}
	class ETLEngineError extends ETLError {}
	class ETLSourceError extends ETLError {}
	class ETLTargetError extends ETLError {}
	class TransformError extends ETLError {}


	// Abstract Classes.

	abstract class ETLBase {

		public function __get($name) {
			$var = trim($name, '_');
			if (isset($this->$var))
				return $this->$var;
			return null;
		}


		protected static function _ct($var) {
			
			static $bool = array('true'		=> true
								,'yes'		=> true
								,'false'	=> false
								,'no'		=> false
								);
			
			if (is_string($var)) {
				if (substr($var, 0, 1) != '+') {
					if (is_numeric($var)) {
						if (strpos($var, '.') !== false)
							return floatval($var);
						return intval($var);
					}
					$val = strtolower($var);
					if (isset($bool[$val]))
						return $bool[$val];
				}
			}
			
			return $var;
			
		}

		protected static function _uq($var) {
			if (substr($var, 0, 1) == '"')
				return trim($var, '"');
			elseif (substr($var, 0, 1) == "'")
				return trim($var, "'");
			return $var;
		}
		
		
		protected static function _dp($data) {
			if (ETL_DEBUG_LEVEL >= ETL_DEBUG_PRINT) {
				if (is_array($data)) {
					echo " ==================== \n";
					foreach ($data as $key => $value) {
						if (is_array($value)) {
							echo " *** $key *** \n";
							foreach ($value as $k => $v)
								echo "$k: $v\n";
							echo " ---------- \n";
						}
						else {
							echo "$key: $value\n";
						}
					}
				}
				else {
					echo "$data\n";
				}
			}
		}
		
	}

	abstract class ETLObject extends ETLBase implements ETLInterface {
		
		
		protected $parent = null;
		protected $config = null;
		
		protected $prepared = null;
		
		
		const _PREPARED = '%s: Already prepared.';
		const _NOT_PREP = '%s: Call prepare() before calling this method.';
		
		
		public function __construct($config, $parent=null) {
			$this->config = $config;
			$this->parent = $parent;
			$this->prepared = false;
		}

		
		public function cleanup() {
			$this->prepared = false;
		}

		public function prepare() {
			if ($this->prepared)
				throw new ETLCodingError(sprintf(self::_PREPARED, __METHOD__));
			$this->prepared = true;
		}


	}

	abstract class Transform extends ETLObject implements ETLProcess {
		
		protected $fields = null;
		
		protected $ordered = null;
		protected $firstrow = null;
		
		const _NO_IMPL = '%s: Must override this method in subclass.';
		
		public function cleanup() {
			$this->ordered = null;
			$this->firstrow = null;
			$this->fields = null;
			parent::cleanup();
		}
		
		public function prepare() {
			parent::prepare();
			$this->fields = false;
			$this->firstrow = true;
			$this->ordered = ($this->parent->source->ordered && $this->parent->target->ordered);
		}
		
		public function process($row) {
			throw new ETLNoImplError(sprintf(self::_NO_IMPL, __METHOD__));
		}
		
	}



	// Classes.

	class CString {
		
		protected $value = null;
		protected $length = null;
		
		
		const _NO_FUNC = '%s: Function "%s" not exists.';
		
		
		public function __construct($value = '') {
			$this->value = $value;
			$this->length = strlen($value);
		}
		
		public function __call($name, $args) {
			
			static $strfuncs = array('count'	=> 'substr_count'
									,'find'		=> 'strpos'
									,'getcsv'	=> 'str_getcsv'
									,'index'	=> 'strpos'
									,'isnum'	=> 'is_numeric'
									,'lower'	=> 'strtolower'
									,'lstrip'	=> 'ltrim'
									,'pad'		=> 'str_pad'
									,'parse'	=> 'parse_str'
									,'qpdec'	=> 'quoted_printable_decode'
									,'qpenc'	=> 'quoted_printable_encode'
									,'repeat'	=> 'str_repeat'
									,'rfind'	=> 'strrpos'
									,'rindex'	=> 'strrpos'
									,'rot13'	=> 'str_rot13'
									,'rstrip'	=> 'rtrim'
									,'shuffle'	=> 'str_shuffle'
									,'strip'	=> 'trim'
									,'toarray'	=> 'str_split'
									,'upper'	=> 'strtoupper'
									,'uudec'	=> 'convert_uudecode'
									,'uuenc'	=> 'convert_uuencode'
									,'b64dec'	=> 'base64_decode'
									,'b64enc'	=> 'base64_encode'
									);
			
			if (isset($strfuncs[$name]))
				$func = $strfuncs[$name];
			elseif (function_exists($name))
				$func = $name;
			else
				throw new ETLCodingError(sprintf(self::_NO_FUNC, __METHOD__, $name));
			$count = count($args);

			if ($count == 0)
				return $func($this->value);
			elseif ($count == 1)
				return $func($this->value, $args[0]);
			else
				array_unshift($args, $this->value);
			return call_user_func_array($func, $args);
			
		}
		
		public function __toString() {
			return $this->value;
		}
		
		
		public function capitalize() {
			return ucfirst(strtolower($this->value));
		}
		
		public function center($width, $fillchar=' ') {
			return str_pad($this->value, $width, $fillchar, STR_PAD_BOTH);
		}
		
		public function endswith($suffix, $start=0, $length=null) {
			if ($length !== null)
				$value = substr($this->value, $start, $length);
			else
				$value = $this->value;
			if (is_array($suffix)) {
				foreach ($suffix as $s) {
					$len = strlen($s);
					if (substr($value, -$len, $len) == $s)
						return true;
				}
				return false;
			}
			$len = strlen($suffix);
			return (substr($value, -$len, $len) == $suffix);
		}
		
		public function has($sub, $ci=false) {
			if ($ci)
				return (stripos($this->value, $sub) !== false);
			return (strpos($this->value, $sub) !== false);
		}
		
		public function ireplace($old, $new) {
			return str_ireplace($old, $new, $this->value);
		}
		
		public function ljust($width, $fillchar=' ') {
			return str_pad($this->value, $width, $fillchar, STR_PAD_LEFT);
		}
		
		public function partition($sep) {
			$value = $this->value;
			$pos = strpos($value, $sep);
			if ($pos === false)
				return array($value, '', '');
			return array(substr($value, 0, $pos), $sep, substr($value, $pos + strlen($sep)));
		}
		
		public function replace($old, $new) {
			return str_replace($old, $new, $this->value);
		}
		
		public function rjust($width, $fillchar=' ') {
			return str_pad($this->value, $width, $fillchar, STR_PAD_RIGHT);
		}
		
		public function rpartition($sep) {
			$value = $this->value;
			$pos = strrpos($value, $sep);
			if ($pos === false)
				return array('', '', $value);
			return array(substr($value, 0, $pos), $sep, substr($value, $pos + strlen($sep)));
		}
		
		public function startswith($prefix, $start=0, $length=null) {
			if ($length !== null)
				$value = substr($this->value, $start, $length);
			else
				$value = $this->value;
			if (is_array($prefix)) {
				foreach ($prefix as $p) {
					if (substr($value, $start, strlen($p)) == $p)
						return true;
				}
				return false;
			}
			return (substr($value, $start, strlen($prefix)) == $prefix);
		}
		
		public function split($sep, $maxsplit=null) {
			if ($maxsplit === null)
				return explode($sep, $this->value);
			return explode($sep, $this->value, $maxsplit);
		}
		
		public function title() {
			return ucwords(strtolower($this->value));
		}
		
		
		public static function join($pieces, $glue=',', $class=false) {
			if ($class) {
				$value = implode($glue, $pieces);
				return new CString($value);
			}
			return implode($glue, $pieces);
		}
		
	}


	class ETLHooks {

		const _UNDEF = '%s: Hook "%s" is not defined.';
		
		protected $_hooks = null;
		
		
		public function __construct($hooks=null) {
			$_hooks = array();
			if (is_array($hooks)) {
				foreach($hooks as $hook)
					$_hooks[$hook] = null;
			}
			$this->_hooks = $_hooks;
		}
		
		
		public function __call($name, $args) {
			if (isset($this->_hooks[$name])) {
				$func = $this->_hooks[$name];
				return call_user_func_array($func, $args);
			}
			throw new ETLCodingError(sprintf(self::_UNDEF, __METHOD__, $name));
		}
		
		
		public function __get($name) {
			if (array_key_exists($name, $this->_hooks))
				return $this->_hooks[$name];
			throw new ETLCodingError(sprintf(self::_UNDEF, __METHOD__, $name));
		}
		
		public function __set($name, $value) {
			if (!array_key_exists($name, $this->_hooks))
				throw new ETLCodingError(sprintf(self::_UNDEF, __METHOD__, $name));
			$this->_hooks[$name] = $value;
		}
		
		
		public function get($name) {
			return isset($this->_hooks[$name]) ? $this->_hooks[$name] : null;
		}
		
		public function set($name, $value) {
			$this->_hooks[$name] = $value;
		}
		
		
	}

	class ETLConfig {
		
		const UNDEF = '%s: Key "%s" not defined.';
		const EXIST = '%s: Key "%s" already exist.';
		
	    const UNDEFINED = '%s: Option "%s" is not found in config.';
	    const EMPTY_VAR = '%s: Empty value provided for "%s" in config.';
	    const NO_NUMBER = '%s: Value for the "%s" is not a number.';
	    
	    const NOVARTYPE = "%s: Don't know how to convert '%s' to %s.";
	    
	    const NOSECTION = '%s: Section "%s" is not found in config.';
	    const NOT_ARRAY = '%s: Expected section, but %s is a value.';
		
		const NO_METHOD = '%s: Unable to call undefined method "%s".';
		const NO_PARAMS = '%s: Unable to call "%s" without arguments.';
		
		
		protected $data = null;
		
		
		public function __construct($items = array()) {
			$this->data = $items;
		}
		
		
		public function __call($name, $args) {
			
			static $vartypes = array('get_bool'		=> 'bool'
									,'get_float'	=> 'float'
									,'get_int'		=> 'int'
									,'get_list'		=> 'list'
									,'get_long'		=> 'long'
									,'get_num'		=> 'num'
									,'get_sec'		=> 'sec'
									,'get_str'		=> 'str'
									,'get_text'		=> 'text'
									,'get_word'		=> 'word'
									);
			
			
			if (isset($vartypes[$name])) {

				$count = count($args);

				if ($count) {
					$func = array($this, $name);
					$type = $vartypes[$name];
					if ($count > 2)
						$args = array_merge(array_splice($args, 0, 2, $type), array_slice($args, 2));
					elseif ($count == 1)
						$args = array_merge($args, array(null, $type));
					else
						$args[] = $type;
					return call_user_func_array($func, $args);
				}

				throw new ETLCodingError(sprintf(self::NO_PARAMS, __METHOD__, $name));
				
			}
			
			throw new ETLCodingError(sprintf(self::NO_METHOD, __METHOD__, $name));
			
		}
		
		public function __get($name) {
			if (isset($this->data[$name]))
				return $this->data[$name];
			throw new ETLConfigError(sprintf(self::UNDEF, __METHOD__, name));
		}
		
		public function __set($name, $value) {
			if (!isset($this->data[$name]))
				throw new ETLConfigError(sprintf(self::UNDEF, __METHOD__, name));
			$this->data[$name] = $value;
		}
		
		
		public function __isset($name) {
			return isset($this->data[$name]);
		}
		
		public function __unset($name) {
			if (!isset($this->data[$name]))
				throw new ETLConfigError(sprintf(self::UNDEF, __METHOD__, name));
			unset($this->data[$name]);
		}
		
		
		public function add($name, $value) {
			if (isset($this->data[$name]))
				throw new ETLConfigError(sprintf(self::EXIST, __METHOD__, name));
			$this->data[$name] = $value;
		}
		
		public function del($name) {
			unset($this->data[$name]);
		}
		
		public function get($name, $default=null) {
			return array_key_exists($this->data[$name]) ? $this->data[$name] : $default;
		}
		
		public function has($name) {
			return array_key_exists($this->data[$name]);
		}
		
		public function pop($name, $default=null) {
			if (array_key_exists($this->data[$name])) {
				$value = $this->data[$name];
				unset($this->data[$name]);
				return $value;
			}
			if ($default !== null)
				return $default;
			throw new ETLConfigError(sprintf(self::UNDEF, __METHOD__, name));
		}
		
		public function set($name, $value) {
			$this->data[$name] = $value;
		}
		
		
		public function items() {
			return $this->data;
		}
		
		public function keys() {
			return array_keys($this->data);
		}
		
		public function values() {
			return array_values($this->data);
		}
		
		
		public function get_var($name, $default=null, $type=null, $errmsg=null, $args=null) {
			
			static $strtypes = array('str'		=> 'strval'
									,'string'	=> 'strval'
									);
			
			static $numtypes = array('double'	=> 'floatval'
									,'float'	=> 'floatval'
									,'int'		=> 'intval'
									,'long'		=> 'intval'
									,'single'	=> 'floatval'
									);
			
			static $boolvals = array('1'		=> true
									,'0'		=> false
									,'true'		=> true
									,'false'	=> false
									,'yes'		=> true
									,'no'		=> false
									,'on'		=> true
									,'off'		=> false
									);
			
			if (isset($this->data[$name])) {
				$value = $this->data[$name];
				if ($value != '') {
					if ($type === null)
						return $value;
					if (isset($strtypes[$type])) {
						$func = $strtypes($type);
						return $func($value);
					}
					if (isset($numtypes[$type])) {
						if (is_numeric($value)) {
							$func = $numtypes[$type];
							return $func($value);
						}
						$error = self::NO_NUMBER;
					}
					if ($type == 'num') {
						if (is_numeric($value)) {
							if (strpos($value, '.') !== false)
								return floatval($value);
							else
								return intval($value);
						}
						$error = self::NO_NUMBER;
					}
					if ($type == 'bool') {
						$val = strtolower($value);
						if (isset($boolvals[$val]))
							return $boolvals[$val];
						return boolval($value);
					}
					if (($type == 'text') || ($type == 'word')) {
						if (substr($value, 0, 1) == '"')
							$value = trim($value, '"');
						elseif (substr($value, 0, 1) == "'")
							$value = trim($value, "'");
						if ($type == 'word') {
							//TODO: Extract the first word from value and return it.
						}
						return $value;
					}
					if ($type == 'list') {
						$sep = isset($args) ? $args : ',';
						return explode($sep, $value);
					}
					if ($type = 'sec') {
						if (is_array($value))
							return $value;
						$error = self::NOT_ARRAY;
					}
					else {
						$error = false;
					}
				}
				else {
					$error = ($type == 'sec') ? NOT_ARRAY : self::EMPTY_VAR;
				}
			}
			else {
				$error = ($type == 'sec') ? self::NOSECTION : self::UNDEFINED;
			}
			
			if ($error) {
				if ($default !== null)
					return $default;
				if ($errmsg === null)
					$errmsg = sprintf($error, __METHOD__, $name);
				throw new ETLConfigError($errmsg);
			}
			
			$errmsg = sprintf(self::NOVARTYPE, __METHOD__, $name, $type);
			throw new ETLCodingError($errmsg);
			
		}
		
		
		public function get_bool($name, $default=null, $errmsg=null, $args=null) {
			return $this->get_var($name, $default, 'bool', $errmsg, $args);
		}
		
		public function get_int($name, $default=null, $errmsg=null, $args=null) {
			return $this->get_var($name, $default, 'int', $errmsg, $args);
		}
		
		public function get_list($name, $default=null, $errmsg=null, $args=null) {
			return $this->get_var($name, $default, 'list', $errmsg, $args);
		}
		
		public function get_num($name, $default=null, $errmsg=null, $args=null) {
			return $this->get_var($name, $default, 'num', $errmsg, $args);
		}

		public function get_sec($name, $default=null, $errmsg=null, $args=null) {
			return $this->get_var($name, $default, 'sec', $errmsg, $args);
		}
		
		public function get_text($name, $default=null, $errmsg=null, $args=null) {
			return $this->get_var($name, $default, 'text', $errmsg, $args);
		}
		
		public function get_word($name, $default=null, $errmsg=null, $args=null) {
			return $this->get_var($name, $default, 'word', $errmsg, $args);
		}
		
	}


	class ETLLogger extends ETLBase {

		const OFF	= 0;
		const ERROR	= 1;
		const WARN	= 2;
		const INFO	= 3;
		const DEBUG	= 4;

		const LOG_LEVEL = ETL_LOG_LEVEL;

		const NO_ARGS = 'ETLLogger::NO_ARGS';


		public $loglevel = null;
		public $dateformat = null;


		protected $filename = null;
		protected $messages = null;

		protected $lasterror = null;

		protected $_fp = null;


		protected static $status = array(self::OFF		=> 'LOG'
										,self::ERROR	=> 'ERROR'
										,self::WARN		=> 'WARNING'
										,self::INFO		=> 'INFO'
										,self::DEBUG	=> 'DEBUG'
										);

		protected static $_cache = array();


		public function __construct($filename, $loglevel=null) {
			$this->filename = $filename;
			if ($loglevel === null)
				$loglevel = self::LOG_LEVEL;
			$this->loglevel = $loglevel;
			$this->messages = array();
			if ($loglevel > self::OFF) {
				if ($fp = @fopen($filename, 'a')) {
					$this->_fp = $fp;
					$this->messages[] = "OpenSuccess: $filename.";
				}
				else {
					$this->lasterror = error_get_last();
					$this->messages[] = "OpenFail: $filename.";
				}
			}

			$this->dateformat = 'Y-m-d H:i:s';

		}

		public function __destruct() {
			if ($this->_fp) @fclose($this->_fp);
		}


		public function log($message, $type=self::INFO, $args=self::NO_ARGS) {
			if ($this->loglevel >= $type) {
				$date = date($this->dateformat);
				if (empty(self::$status[$type]))
					$status = self::$status[self::OFF];
				else
					$status = self::$status[$type];
				$line = "$date - $status -- $message";
				if ($args !== self::NO_ARGS)
					$line .= ' --- ' . var_export($args, true);
				$r = @fwrite($this->_fp, $line . PHP_EOL);
				if ($r === false) {
					$this->lasterror = error_get_last();
					$this->messages[] = "WriteFail: $line.";
				}
				return $r;
			}
			return true;
		}


		public function debug($message, $args=self::NO_ARGS) {
			return $this->log($message, self::DEBUG, $args);
		}

		public function error($message, $args=self::NO_ARGS) {
			return $this->log($message, self::ERROR, $args);
		}

		public function info($message, $args=self::NO_ARGS) {
			return $this->log($message, self::INFO, $args);
		}

		public function warn($message, $args=self::NO_ARGS) {
			return $this->log($message, self::WARN, $args);
		}


		public static function getInstance($name, $level=null, $prefix='', $suffix='.log') {
			static $logdir = null;
			static $instances = array();
			if (isset($instances[$name]))
				return $instances[$name];
			if ($logdir === null)
				$logdir = realpath(dirname(__FILE__) . '/../log');
			$dirname = $prefix ? $logdir .DS. $prefix : $logdir;
			if ($prefix && !is_dir($dirname))
				mkdir($dirname);
			$filename = $dirname .DS. $name . $suffix;
			$instance = new ETLLogger($filename, $level);
			$instances[$name] = $instance;
			return $instance;
		}

		public static function get($name) {
			if (isset(self::$_cache[$name])) {
				$logger = self::$_cache[$name];
				return $logger;
			}
			return null;
		}

		public static function set($logger, $name = null) {
			if (empty($name))
				$name = basename($logger->filename, '.log');
			self::$_cache[$name] = $logger;
		}

	}

	class ETLRunner extends ETLBase {
		
		protected $maps = null;
		protected $hooks = null;
		
		protected $logger = null;
		protected $logdir = null;

		protected $config = null;

		protected $_check = null;
		
		
		const _FILE_NOT_FOUND = '%s: File "%s" not found.';
		

		public function __construct($names=null, $config=null) {

			$this->logdir = date('Y-m-d-His');
			$this->logger = ETLLogger::getInstance($this->logdir);

			$this->logger->debug(__METHOD__, $names);

			$this->config = $config;

			$this->maps = array();

			if (is_array($names)) {
				foreach ($names as $name) {
					$this->add($name);
				}
			}
			
			$this->hooks = new ETLHooks(array('created', 'prepare', 'cleanup'));
			
		}
		
		
		public function add($name, $replace=true, $config=null) {
			$this->logger->debug(__METHOD__, array($name, $replace));
			if ($replace || !isset($this->maps[$name])) {
				$map = $this->parse($name);
				$this->maps[$name] = $this->merge($name, $map, $config);
				return true;
			}
			return false;
		}
		
		public function get($name, $existing=true, $config=null) {
			$this->logger->debug(__METHOD__, array($name, $existing));
			if ($existing && isset($this->maps[$name]))
				$map = $this->maps[$name];
			else
				$map = $this->parse($name);
			return new ETLEngine($this->merge($nname, $map, $config), $this);
		}
		
		public function log($name, $level = null) {
			return ETLLogger::getInstance($name, $level, $this->logdir);
		}
		
		public function run($writedata=null, $batchsize=null) {

			$this->logger->debug(__METHOD__, array($writedata, $batchsize));

			$hooks = $this->Hooks;
			
			$created = $hooks->created;
			$prepare = $hooks->prepare;
			$cleanup = $hooks->cleanup;

			$results = array();

			foreach ($this->maps as $name => $map) {

				$this->logger->debug(__METHOD__ . ": $name", $map);

				$engine = new ETLEngine($map, $this);

				if ($created) {
					$r = $created('created', $engine);
					if ($r === null)
						break;
					elseif ($r == false)
						continue;
				}
				
				if ($prepare || $cleanup) {

					$r = true;
					
					$engine->prepare();

					if ($prepare)
						$r = $prepare('prepare', $engine);

					if ($r)
						$result = $engine->run($writedata, $batchsize);
					else
						$result = $r;
					
					$this->logger->info(__METHOD__ . ": $name", $result);

					if ($r && $cleanup)
						$r = $cleanup('cleanup', $engine);

					$engine->cleanup();

					if ($r === null)
						break;
					
				}
				else {

					$result = $engine->run($writedata, $batchsize);
					$this->logger->info(__METHOD__ . ": $name", $result);

				}
				
				$results[$name] = $result;
				
			}

			return $results;

		}
		
		
		protected function merge($name, $map, $config=null) {
			if (!empty($this->config)) {
				if (!empty($config))
					$config = array_merge($this->config, $config);
				else
					$config = $this->config;
			}
			if (!empty($config))
				$map = array_merge($config, $map);
			$merged = new ETLConfig();
			foreach($map as $key => $value) {
				if (is_array($value))
					$value = new ETLConfig($value);
				$merged.add($key, $value);
			}
			if (isset($merged['engine']))
				$merged['engine']['name'] = $name;
			return $merged;
		}
		
		protected function parse($name) {

			if (!strpos($name, '.'))
				$name = "$name.ini";
			$file = realpath(dirname(__FILE__) . '/../etc') .DS. $name;
			
			$check = false;
			
			if ($this->_check === null) {
				$this->_check = array();
				$check = true;
			}
			
			if (array_key_exists($file, $this->_check))
				return false;
			
			$this->_check[$file] = $name;
			
			$this->logger->debug(__METHOD__ . "('$name')", $file);
			
			if (!file_exists($file)) {
				$this->logger->error(__METHOD__ . "('$name'): File not found.", $file);
				throw new ETLRunnerError(sprintf(self::_FILE_NOT_FOUND, __METHOD__, $file));
			}
			
			$ini = parse_ini_file($file, true);
			$map = array();
			
			if (!empty($ini['includes'])) {
				foreach ($ini['includes'] as $include) {
					$inc = $this->parse($include);
					if (!empty($inc))
						$map = array_merge($map, $inc);
				}
			}
			
			foreach ($ini as $sec => $opts) {
				if (strpos($sec, ':')) {
					list($sec, $base) = explode(':', $sec);
					if (!empty($map[$base])) {
						if (!empty($map[$sec]))
							$map[$sec] = array_merge($map[$sec], $map[$base]);
						else
							$map[$sec] = $map[$base];
					}
				}
				if (!empty($map[$sec]))
					$map[$sec] = array_merge($map[$sec], $opts);
				else
					$map[$sec] = $opts;
			}
			
			if ($check)
				$this->_check = null;
			
			return $map;

		}
		
	}

	class ETLEngine extends ETLObject {

		const BATCH_SIZE = ETL_BATCH_SIZE;
		const WRITE_DATA = ETL_WRITE_DATA;

		public $batchsize = null;
		public $writedata = null;

		protected $name = null;
		protected $hooks = null;

		protected $logger = null;

		protected $source = null;
		protected $target = null;
		
		protected $_procs = null;

	    const NO_CLASS = '%s: Class "%s" not found.';
	    
	    const NOENGINE = 'Engine settings not found.';
	    const NOSECTON = 'Settings for "%s" not found.';


		public function __construct($config, $parent=null) {

			parent::__construct($config, $parent);

			$engine = $config->get_sec('engine', null, self::NOENGINE);
			$this->name = $name = $config->get_word('name', 'etl_engine');
			
			$this->logger = $parent ? $parent->log($name) : ETLLogger::getInstance($name);

			$this->writedata = $engine->get_bool('writedata', self::WRITE_DATA);
			$this->batchsize = $engine->get_int('batchsize', self::BATCH_SIZE);

			
			$this->source = $this->_($engine->get_word('source'), 'source');
			$this->target = $this->_($engine->get_word('target'), 'target');
			
			$process = $config->get_sec('process', false);
			
			$procs = array();
			if ($process) {
				foreach ($process->keys() as $proc) {
					$class = $process->get_word($proc);
					$procs[$proc] = $this->_($class, $proc);
				}
			}
			$this->_procs = $procs;
			
			$this->hooks = new ETLHooks(array('postread', 'pretrans', 'postproc', 'prewrite', 'writeres'));
			
			$this->logger->debug(__METHOD__, count($procs));
			
		}


		public function cleanup() {
			$this->logger->info(__METHOD__);
			foreach (array_reverse($this->_procs) as $proc)
				$proc->cleanup();
			$this->target->cleanup();
			$this->source->cleanup();
			parent::cleanup();
		}

		public function prepare() {
			parent::prepare();
			$this->source->prepare();
			$this->target->prepare();
			foreach ($this->_procs as $proc)
				$proc->prepare();
			$this->logger->info(__METHOD__);
		}

		public function run($writedata=null, $batchsize=null) {

			$cleanup = false;
			if (!$this->prepared) {
				$this->prepare();
				$cleanup = true;
			}

			$this->logger->info(__METHOD__, array($writedata, $batchsize));

			$source = $this->source;
			$target = $this->target;
			
			if ($writedata === null)
				$writedata = $this->writedata;
			else
				$writedata = boolval($writedata);
			
			if ($batchsize === null)
				$batchsize = $this->batchsize;
			else
				$batchsize = intval($batchsize);
			
			$this->logger->debug(__METHOD__, array($writedata, $batchsize));

			$procs = $this->_procs;
			$hooks = $this->_hooks;

			$postread = $hooks->postread;
			$pretrans = $hooks->pretrans;
			$postproc = $hooks->postproc;
			$prewrite = $hooks->prewrite;
			$writeres = $hooks->writeres;

			$result = array();
			
			while ($rows = $source->read($batchsize)) {
				
				$numrows = count($rows);
				
				// Run post-read hook if set.
				if ($postread) {
					$runok = $postread('postread', $rows);
					if ($runok === null)
						break;
					if (is_array($runok)) {
						$rows = $runok;
						$runok = true;
					}
				}
				else {
					$runok = true;
				}
				
				if ($runok) {
					
					// Transform each row one-by-one.
					if ($procs || $pretrans || $postproc) {
						$_rows = array();
						$indexes = array();
						foreach ($rows as $idx => $row) {
							// Run pre-trans hook.
							if ($pretrans) {
								$runok = $pretrans('pretrans', $row);
								if ($runok === null)
									break;
								if (is_array($runok)) {
									$row = $runok;
									$runok = true;
								}
							}
							else{
								$runok = true;
							}
							if ($runok && $procs) {
								// Run transformations.
								foreach ($procs as $name => $proc) {
									$row = $proc->process($row);
									if (!$row)
										continue;
								}
								// Run post-proc hook.
								if ($postproc) {
									$runok = $postproc('postproc', $row);
									if ($runok === null)
										break;
									if ($runok == false)
										continue;
									if (is_array($runok)) {
										$row = $runok;
										$runok = true;
									}
								}
							}
							$_rows[] = $row;
							$indexes[] = $idx;
						}
						# End foreach ($rows as $idx => $row):
						if (!empty($_rows)) {
							$runok = true;
							if (count($_rows) == $numrows)
								$indexes = false;
							$rows = $_rows;
						}
						else {
							$runok = false;
							$rows = array();
						}
					}
					else {
						$indexes = false;
					}
					# End if ($procs || $pretrans || $postproc): Transform rows.
					
					if ($runok) {
						
						if ($prewrite) {
							$runok = $prewrite('prewrite', $rows);
							if ($runok === null)
								break;
							if (is_array($runok)) {
								$rows = $runok;
								$runok = true;
							}
						}
						
						if ($runok) {
							
							if (ETL_DEBUG_LEVEL >= ETL_DEBUG_PRINT)
								$this->_dp($rows);
							
							// Write data.
							if ($writedata) {
								$res = $target->write($rows);
								if ($writeres) {
									$runok = writeres('writeres', $res);
									if (is_array($runok)) {
										$res = $runok;
										$runok = true;
									}
								}
								if (array_key_exists('ids', $res)) {
									if ($res['ids'] !== false)
										$ids = $res['ids'];
									else
										$ids = array_fill(0, $numrows, false);
								}
								else {
									$ids = $res;
								}
								if ($indexes) {
									$list = array_fill(0, $numrows, null);
									foreach ($ids as $i => $id)
										$list[$indexes[$i]] = $id;
									$result = array_merge($result, $list);
								}
								else {
									$result = array_merge($result, $ids);
								}
								if ($runok === null)
									break;
								continue;
							}
							// end if ($writedata):
							
						}
						// End if ($runok): prewrite hook.
						
					}
					// End if ($runok): process/transform
					
				} 
				// End if ($runok): postread hook.
				
				$ids = array_fill(0, $numrows, null);
				$result = array_merge($result, $ids);
				
			}
			// End while ($rows = $source->read($batchsize)):

			$this->logger->info(__METHOD__, "Result: $result");
			
			if ($cleanup)
				$this->cleanup();
			
			return $result;
			
		}


		protected function _($class, $section) {
			if (!class_exists($class)) {
				$name = strtolower(preg_replace('/(Source|Target)/', 'Engine', $class));
				require_once(dirname(__FILE__) . "/$name.php");
			}
			if (!class_exists($class))
				throw new ETLConfigError(sprintf(self::NO_CLASS, __METHOD__, $class));
			$config = $this->config->get_sec($section, null, sprintf(self::NOSECTON, $section));
			return new $class($config, $this);
		}


	}


	// Transforms.

	class TConverter extends Transform {
		
		public function process($row) {
			
			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));
			
			if ($this->firstrow)
				$this->init($row);

			if (!empty($this->fields)) {
				foreach ($this->fields as $field => $type) {
					settype($row[$field], $type);
				}
			}

			return $row;

		}
		
		protected function init($row) {

			static $types = array('boolean', 'float', 'integer', 'null', 'string');

			$fields = array();
			$config = $this->config;

			foreach ($config->keys() as $field) {
				if (array_key_exists($field, $row)) {
					$type = $config->get_word($field);
					if (in_array($type, $types))
						$fields[$field] = $type;
				}
			}

			$this->fields = $fields;
			$this->firstrow = false;
			
		}
		
	}

	class TFieldsMap extends Transform {
		
		protected $values = null;
		
		public function process($row) {

			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));

			if ($this->firstrow)
				$this->init($row);

			if (!empty($this->fields)) {
				$r = array();
				$values = $this->values;
				foreach ($this->fields as $field => $action) {
					if (is_array($action)) {
						$value = array();
						foreach ($action as $i)
							$value[] = $row[$i];
						$r[$field] = implode($values[$field], $value);
					}
					else {
						if (isset($values[$field]))
							$value = $values[$field];
						else
						 	$value = $row[$action];
						$r[$field] = $value;
					}
				}
				return $r;
			}

			return $row;

		}
		
		protected function init($row) {
			
			$fields = array_keys($row);
			if ($ordered = $this->ordered) {
				$torder = $dorder = array();
				$sorder = array_flip($fields);
				$order = 0;
			}
			$fields = array_combine($fields, $fields);
			
			$values = array();
			$config = $this->config;
			
			foreach ($config->keys() as $field) {
				$action = $config->get_text($field);
				if ($action == '-') {
					if ($ordered)
						$dorder[] = $field;
					unset($fields[$field]);
				}
				elseif (strpos($action, ',')) {
					if (strpos($action, ':')) {
						$value = explode(':', $action);
						$parts = explode(',', $value[0]);
						$values[$field] = $value[1];
						if (!empty($value[2]) && ($value[2] == '-')) {
							foreach ($parts as $part) {
								if ($ordered)
									$dorder[] = $part;
								unset($fields[$part]);
							}
						}
					}
					else {
						$parts = explode(',', $action);
						$values[$field] = ' ';
					}
					if ($ordered) {
						$torder[$field] = $sorder[$parts[0]];
						$order = $sorder[$parts[1]];
					}
					$fields[$field] = $parts;
				}
				elseif (substr($action, 0, 1) == '+') {
					if ($ordered && $order) {
						$torder[$field] = $order;
						$order = 0;
					}
					$value = substr($action, 1);
					$value = self::_ct($value);
					$fields[$field] = $field;
					$values[$field] = $value;
				}
				elseif (substr($action, 0, 1) == '-') {
					$action = substr($action, 1);
					if ($ordered) {
						$torder[$field] = $sorder[$action];
						$dorder[] = $action;
						$order = 0;
					}
					unset($fields[$action]);
					$fields[$field] = $action;
				}
				else {
					if ($ordered && $order) {
						$torder[$field] = $order;
						$order = 0;
					}
					$fields[$field] = $action;
				}
			}

			if ($ordered) {
				$forder = array_merge($sorder, $torder);
				foreach ($dorder as $order)
					unset($forder[$order]);
				if (asort($forder))
					$this->fields = array_merge($forder, $fields);
				else
					$this->fields = $fields;
			}
			else {
				$this->fields = $fields;
			}

			$this->values = $values;
			$this->firstrow = false;

		}
		
	}

	class TNullValue extends Transform {
		
		public function process($row) {

			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));

			if ($this->firstrow)
				$this->init($row);

			if (!empty($this->fields)) {
				foreach ($this->fields as $field => $value) {
					if ($row[$field] == null) {
						$row[$field] = $value;
					}
				}
			}

			return $row;

		}
		
		protected function init($row) {

			$fields = array();
			$config = $this->config;
			
			if ($config->has('*')) {
				$value = $config->get('*');
				foreach (array_keys($row) as $field) {
					$fields[$field] = $value;
				}
			}

			foreach ($config->items() as $field => $value) {
				if (array_key_exists($field, $row)) {
					$fields[$field] = $value;
				}
			}

			$this->fields = $fields;
			$this->firstrow = false;
			
		}
		
	}

	class TTrimValue extends Transform {
		
		public function process($row) {

			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));

			if ($this->firstrow)
				$this->init($row);

			if (!empty($this->fields)) {
				foreach ($this->fields as $field => $func) {
					$row[$field] = $func($row[$field]);
				}
			}

			return $row;

		}
		
		protected function init($row) {
			
			static $funcs = array('trim', 'ltrim', 'rtrim');
			
			$fields = array();
			$config = $this->config;
			
			foreach ($config->keys() as $field) {
				if (array_key_exists($field, $row)) {
					$func = $config->get_word($field);
					if (in_array($func, $funcs))
						$fields[$field] = $func;
				}
			}
			
			$this->fields = $fields;
			$this->firstrow = false;
			
		}
		
	}


	class TSingleMap extends Transform {

		protected $lookup = null;


		const _NO_PARENT = '%s: Unexpected NULL for parent.';


		public function __construct($config, $parent=null) {
			if ($parent === null)
				throw new ETLCodingError(sprintf(self::_NO_PARENT, __METHOD__));
			parent::__construct($config, $parent);
		}


		public function cleanup() {
			$this->lookup = null;
			parent::cleanup();
		}

		public function prepare() {

			parent::prepare();

			$lookup = array();
			$fields = array();

			$config = $this->config;
			$parent = $this->parent;

			foreach ($config->keys() as $key) {
				
				$value = $config->get_text($key);
				
				$parts = explode(':', $value, 3);
				$field = !empty($parts[0]) ? $parts[0] : $key;

				$object = !empty($parts[1]) ? $parts[1] : 'target';
				$func = array($parent->$object, 'lookup');
				$args = explode(',', $parts[2], 4);

				$lookup[$key] = call_user_func_array($func, $args);
				$fields[$key] = $field;

			}

			$this->lookup = $lookup;
			$this->fields = $fields;

		}

		public function process($row) {

			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));

			$lookup = $this->lookup;

			foreach ($this->fields as $key => $field) {
				$value = $row[$field];
				if (isset($lookup[$key][$value]))
					$row[$key] = $lookup[$key][$value];
				elseif (($key != $field) && !isset($row[$key]))
					$row[$key] = $value;
			}
			return $row;

		}

	}

	class TDoubleMap extends Transform {

		protected $slookup = null;
		protected $tlookup = null;


		const _NO_PARENT = '%s: Unexpected NULL for parent.';


		public function __construct($config, $parent=null) {
			if ($parent === null)
				throw new ETLCodingError(sprintf(self::_NO_PARENT, __METHOD__));
			parent::__construct($config, $parent);
		}


		public function cleanup() {
			$this->tlookup = null;
			$this->slookup = null;
			parent::cleanup();
		}

		public function prepare() {

			parent::prepare();

			$fields = array();

			$slookup = array();
			$tlookup = array();

			$config = $this->config;
			$parent = $this->parent;

			foreach ($config->keys() as $key) {
				
				$value = $config->get_text($key);
				
				$parts = explode(':', $value, 3);
				$field = !empty($parts[0]) ? $parts[0] : $key;

				$func = array($parent->source, 'lookup');
				$args = explode(',', $parts[1], 4);
				$slookup[$key] = call_user_func_array($func, $args);

				$func = array($parent->target, 'lookup');
				$args = explode(',', $parts[2], 4);
				$tlookup[$key] = call_user_func_array($func, $args);

				$fields[$key] = $field;

			}

			$this->slookup = $slookup;
			$this->tlookup = $tlookup;
			
			$this->fields = $fields;
			
		}

		public function process($row) {

			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));

			$slookup = $this->slookup;
			$tlookup = $this->tlookup;

			foreach ($this->fields as $key => $field) {
				$value = $row[$field];
				if (empty($value)) {
					if (($key != $field) && !isset($row[$key]))
						$row[$key] = $value;
				}
				elseif (strpos($value, ',')) {
					$ids = array();
					foreach (explode(',', $value) as $id) {
						$name = $slookup[$key][$id];
						$ids[] = $tlookup[$key][$name];
					}
					$row[$key] = implode(',', $ids);
				}
				else {
					$name = $slookup[$key][$value];
					$row[$key] = $tlookup[$key][$name];
				}
			}

			return $row;

		}

	}

	

?>