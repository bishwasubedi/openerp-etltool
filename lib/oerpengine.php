<?php


	require_once(dirname(__FILE__) . '/etlengine.php');


	abstract class OERPObject extends ETLObject {

		protected $oerp = null;		// OpenObjectRpc object.
		protected $model = null;	// Model Name
		protected $object = null;	// OpenObjectRpcObject object.

		protected $fields = null;	// Fields to read/write.
		protected $ftypes = null;	// Field Types.
		
		protected $ordered = false;	// Is fields are ordered?
		
		protected $rel_fields = null;	// Relational Fields.
		protected $rel_ftypes = null;	// Relational field Types.

		protected $firstrun = null;	// First Run.

		protected $many2one = null;	// Many2One field type.
		protected $one2many = null;	// One2Many field type.
		
		
		const _OERP_CLASS = 'OpenObjectRpc';
		const _CLASS_FILE = '/../../phpclient/openobject.php';
		
		const _NO_CLASS = '%s: RPC client class "%s" not found.';
		const _NO_LOGIN = '%s: Unable to login, DB: %s, User: %s.';


		public function __construct($config, $parent=null) {

			parent::__construct($config, $parent);
			
			$class = self::_OERP_CLASS;
			
			if (!class_exists($class))
				require_once(dirname(__FILE__) . self::_CLASS_FILE);
			if (!class_exists($class))
				throw new ETLCodingError(sprintf(self::_NO_CLASS, __METHOD__, $class));
			
			$location = $config->get_text('location', '');
			$this->oerp = $location ? new $class($location) : new $class();
			
			$this->model = $config->get_word('model');
			$this->database = $config->get_word('database');
			
			$fields = array();
			$ftypes = array();
			
			$fields_str = $config->get_text('fields', '');
			if (!empty($fields_str)) {
				$all_fields = (substr($fields_str, 0, 1) == ':');
				if ($all_fields)
					$fields_str = substr($fields, 1);
				foreach (explode(',', $fields_str) as $field) {
					if (strpos($field, '.')) {
						list($field, $ftype) = explode('.', $field);
						$ftypes[$field] = $ftype;
					}
					if (!$all_fields)
						$fields[] = $field;
				}
			}

			$this->fields = $fields;
			$this->ftypes = $ftypes;
			
		}


		public function cleanup() {

			$this->object = null;

			$this->rel_ftypes = null;
			$this->rel_fields = null;

			$this->firstrun = null;

			parent::cleanup();

		}

		public function lookup($object, $key, $value, $filter = '') {

			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));
			
			if (strpos($key, '.'))
				list($key, $k) = explode('.', $key, 2);
			else
				$k = false;
			
			if (strpos($value, '.'))
				list($value, $v) = explode('.', $value, 2);
			else
				$v = false;
			
			$result = array();
			$search = !empty($filter) ? self::parse($filter) : null;
			$fields = array($key, $value);
			
			$rows = $this->object->readall($object, $search, $fields);
			
			if ($k && $v) {
				$k = ($k == 'name') ? 1 : 0;
				$v = ($v == 'name') ? 1 : 0;
				foreach ($rows as $row)
					$result[$row[$key][$k]] = $row[$value][$v];
			}
			elseif ($k) {
				$k = ($k == 'name') ? 1 : 0;
				foreach ($rows as $row)
					$result[$row[$key][$k]] = $row[$value];
			}
			elseif ($v) {
				$v = ($v == 'name') ? 1 : 0;
				foreach ($rows as $row)
					$result[$row[$key]] = $row[$value][$v];
			}
			else {
				foreach ($rows as $row)
					$result[$row[$key]] = $row[$value];
			}
			
			return $result;

		}

		public function prepare() {

			parent::prepare();
			
			$user = $this->config->get_text('username', 'admin');
			$pass = $this->config->get_text('password', 'admin');

			$id = $this->oerp->login($this->database, $user, $pass);

			if (!$id)
				throw new ETLError(sprintf(self::_NO_LOGIN, __METHOD__, $this->database, $user));

			$this->object = $this->oerp->object;

			$this->rel_fields = false;
			$this->rel_ftypes = false;

			$this->firstrun = true;

		}


		protected function init($row, $_m2o, $_o2m) {


			static $ft_m2o = array('id' => 0, 'name' => 1);
			static $ft_o2m = array('add' => array(',', false), 'set' => array(',', true));


			$many2one = $this->many2one;
			$one2many = $this->one2many;

			$fields = $this->object->fields($this->model, $this->fields);
			$ftypes = $this->ftypes;
			
			$rel_fields = array();

			if ($row && empty($this->fields)) {
				foreach ($fields as $name => $field) {
					$type = $field['type'];
					if (strpos($type, '2')) {
						if (isset($row[$name])
							|| (($type == 'many2one')
								&& isset($row[$name.'_id'])
								&& isset($row[$name.'_name']))) {
							$rel_fields[$name] = $type;
						}
					}
				}
			}
			else {
				foreach ($fields as $name => $field) {
					$type = $field['type'];
					if (strpos($type, '2')) {
						$rel_fields[$name] = $type;
					}
				}
			}

			$m2o = array();
			$o2m = array();
			
			foreach ($rel_fields as $name => $type) {
				
				$ftype = null;
				
				if (!empty($ftypes[$name])) {
					$ftype = $ftypes[$name];
					if ($type == 'many2one') {
						if (!in_array($ftype, $_m2o))
							$ftype = $many2one;
					}
					else {
						if (!in_array($ftype, $_o2m))
							$ftype = $one2many;
					}
				}
				
				if ($ftype === null)
					$ftype = ($type == 'many2one') ? $many2one : $one2many;
				
				if ($row === false) {
					if (isset($ft_m2o[$ftype]))
						$m2o[$name] = $ft_m2o[$ftype];
					elseif ($ftype == 'both')
						$m2o[$name] = array($name.'_id', $name.'_name');
					elseif ($ftype == 'join')
						$o2m[$name] = ',';
				}
				else {
					if (isset($ft_o2m[$ftype]))
						$o2m[$name] = $ft_o2m[$ftype];
					elseif ($ftype == 'id')
						$m2o[$name] = array(0, $name.'_id', $name.'_name');
					elseif ($ftype == 'name')
						$m2o[$name] = array(1, $name.'_name', $name.'_id');
				}
				
			}
			
			$rel_ftypes = array();
			
			if (count($m2o))
				$rel_ftypes['m2o'] = $m2o;
			if (count($o2m))
				$rel_ftypes['o2m'] = $o2m;
			
			$this->rel_fields = $rel_fields;
			$this->rel_ftypes = $rel_ftypes;
			
			$this->firstrun = false;
			
		}

		protected function init_rel($m2o_arr, $o2m_arr, $m2o_def, $o2m_def) {
			$m2o = $this->config->get_word('many2one', $m2o_def);
			$o2m = $this->config->get_word('one2many', $o2m_def);
			$this->many2one = in_array($m2o, $m2o_arr) ? $m2o : $m2o_def;
			$this->one2many = in_array($o2m, $o2m_arr) ? $o2m : $o2m_def;
		}


		protected static function parse($search, $ts = '+', $fs = ',') {
			if (is_array($search))
				return $search;
			if (substr($search, 0, 1) == '"')
				$search = trim($search, '"');
			elseif (substr($search, 0, 1) == "'")
				$search = trim($search, "'");
			$domain = array();
			foreach (explode($ts, $search) as $item) {
				if (strpos($item, $fs)) {
					$parts = explode($fs, $item);
					$index = count($parts) - 1;
					$value = self::_ct($parts[$index]);
					$parts[$index] = $value;
					$domain[] = $parts;
				}
				else {
					$domain[] = $item;
				}
			}
			return $domain;
		}


	}


	class OERPSource extends OERPObject implements ETLSource {
		
		const MANY2ONE = 'both';
		const ONE2MANY = 'join';
		
		protected $offset = null;	// Read offset.
		protected $search = null;	// Search Domain.
		
		protected static $_m2o = array('raw', 'id', 'name', 'both');
		protected static $_o2m = array('raw', 'join');

		
		public function __construct($config, $parent=null) {
			
			parent::__construct($config, $parent);
			
			$search = $config->get_text('search', '');
			if (!empty($search))
				$this->search = self::parse($search);

			$this->init_rel(self::$_m2o, self::$_o2m, self::MANY2ONE, self::ONE2MANY);
			
		}


		public function cleanup() {
			$this->offset = null;
			parent::cleanup();
		}

		public function lookup($object, $key = 'id', $value = 'name', $filter = '') {
			return parent::lookup($object, $key, $value, $filter);
		}

		public function prepare() {
			parent::prepare();
			$this->offset = 0;
		}

		public function read($size) {
			
			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));
			
			if ($this->firstrun)
				$this->init(false, self::$_m2o, self::$_o2m);
			
			$limit = ($size >= 0) ? $size : null;
			$rows = $this->object->page($this->model, $this->search, $this->fields, $this->offset, $limit);
			$this->offset += count($rows);
			
			if (!empty($this->rel_ftypes)) {
				$rel_ftypes = $this->rel_ftypes;
				$m2o = isset($rel_ftypes['m2o']) ? $rel_ftypes['m2o'] : false;
				$o2m = isset($rel_ftypes['o2m']) ? $rel_ftypes['o2m'] : false;
				$num = count($rows);
				for ($i = 0; $i < $num; $i++) {
					$row = $rows[$i];
					if ($m2o) {
						foreach ($m2o as $name => $field) {
							if (is_array($field)) {
								$row[$field[0]] = $row[$name][0];
								$row[$field[1]] = $row[$name][1];
								unset($row[$name]);
							}
							else {
								$row[$name] = $row[$name][$field];
							}
						}
					}
					if ($o2m) {
						foreach ($o2m as $name => $glue) {
							$row[$name] = implode($glue, $row[$name]);
						}
					}
					$rows[$i] = $row;
				}
			}

			return $rows;
			
		}

	}

	class OERPTarget extends OERPObject implements ETLTarget {

		const MANY2ONE = 'raw';
		const ONE2MANY = 'set';

		protected $import = null;	// Builk load/import.

		protected $create = null;	// Create new.
		protected $update = null;	// Update existing.

		protected $column = null;	// Lookup field for update.
		protected $filter = null;	// Search filter for lookup.
		
		protected $dryrun = null;	// Do not write anything.
		
		protected $_fields = false;	// Fields to write.

		protected $_update_lookup = null;	// Lookup table for update.
		protected $_update_column = null;	// Lookup field for update.


		protected static $_m2o = array('raw', 'id', 'name');
		protected static $_o2m = array('raw', 'add', 'set');


		public function __construct($config, $parent=null) {
			
			parent::__construct($config, $parent);
			
			if (!empty($this->fields))
				$this->_fields = array_flip($this->fields);
			
			$this->import = $config->get_bool('import', true);
			$this->create = $config->get_bool('create', true);
			$this->update = $config->get_bool('update', false);
			
			$this->column = $config->get_word('column', 'name');
			$this->filter = $config->get_text('filter', '');

			$this->dryrun = $config->get_bool('dryrun', false);
			
			$this->init_rel(self::$_m2o, self::$_o2m, self::MANY2ONE, self::ONE2MANY);
			
		}


		public function cleanup() {
			if (!$this->import && $this->update)
				$this->_update_lookup = null;
				$this->_update_column = null;
			parent::cleanup();
		}

		public function lookup($object, $key = 'name', $value = 'id', $filter = '') {
			return parent::lookup($object, $key, $value, $filter);
		}

		public function prepare() {
			parent::prepare();
			if (!$this->import && $this->update) {
				$column = $this->column;
				if (strpos($column, '.'))
					$this->_update_column = explode('.', $column)[0];
				else
					$this->_update_column = $column;
				$this->_update_lookup = $this->Lookup($this->model, $column, 'id', $this->filter);
			}
		}

		public function write($rows) {
			
			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));
			
			if ($this->firstrun)
				$this->init($rows[0], self::$_m2o, self::$_o2m);
			
			if (!$this->import)
				return $this->_write($rows);
			
			$fields = $this->_fields;
			
			if (!empty($this->fields))
				$flds = $this->fields;
			else
				$flds = False;
			$data = array();
			
			$rel = !empty($this->rel_ftypes);
			
			foreach ($rows as $row) {
				if ($rel)
					$row = $this->_rel($row);
				if ($fields)
					$row = array_intersect_key($row, $fields);
				if (!$flds)
					$flds = array_keys($row);
				$values = array_values($row);
				$data[] = array_map('strval', $values);
			}
			
			if ($this->dryrun)
				$res = array('ids' => array_fill(0, count($data), true), 'messages' => array('dryrun'));
			else
				$res = $this->object->load($this->model, $flds, $data);
			if (ETL_DEBUG_LEVEL > ETL_DEBUG_PRINT) {
				if ($res['ids'] != false)
					echo 'Import Success * Result: ' . print_r($res['ids'], true);
				else
					echo 'Import Failed! * Errors: ' . print_r($res['messages'], true);
			}
			return $res;
			
		}


		protected function _write($rows) {
			
			$model = $this->model;
			$object = $this->object;
			
			$create = $this->create;
			if ($update = $this->update) {
				$column = $this->_update_column;
				$lookup = $this->_update_lookup;
			}
			
			$dryrun = $this->dryrun;
			$fields = $this->_fields;

			$rel = !empty($this->rel_ftypes) ? true : false;
			
			$ids = array(); $res = null;
			
			foreach ($rows as $row) {

				if ($rel)
					$row = $this->_rel($row);

				if ($fields)
					$row = array_intersect_key($row, $fields);

				if ($update && isset($lookup[$row[$column]])) {
					$id = $lookup[$row[$column]];
					if ($dryrun) {
						$res = $id;
					}
					else {
						$res = $object->write($model, $id, $row);
						if ($res)
							$res = $id;
					}
					if (ETL_DEBUG_LEVEL > ETL_DEBUG_PRINT)
						echo "Update * Result: $res * Row: " . print_r($row, true);
					$ids[] = $res;
					continue;
				}
				
				if ($create) {
					if ($dryrun)
						$res = true;
					else
						$res = $object->create($model, $row);
					if (ETL_DEBUG_LEVEL > ETL_DEBUG_PRINT)
						echo "Create * Result: $res * Row: " . print_r($row, true);
					$ids[] = $res;
				}
				
			}

			return $ids;
			
		}

		protected function _rel($row) {

			$rel_ftypes = $this->rel_ftypes;

			if (isset($rel_ftypes['m2o'])) {
				foreach ($rel_ftypes['m2o'] as $name => $field) {
					if (is_array($row[$name])) {
						$row[$name] = $row[$name][$field[0]];
					}
					else {
						$row[$name] = $row[$field[1]];
						unset($row[$field[1]]);
						unset($row[$field[2]]);
					}
				}
			}

			if (isset($rel_ftypes['o2m'])) {
				foreach ($rel_ftypes['o2m'] as $name => $field) {
					$values = $row[$name];
					if (!is_array($values))
						$values = explode($field[0], $values);
					if ($field[1]) {
						$list = array(array(6, 0, $values));
						$row[$name] = $list;
					}
					else {
						$list = array();
						foreach ($values as $value)
							$list[] = array(4, $value);
						$row[$name] = $list;
					}
				}
			}

			return $row;

		}

	}

	
	class TSMapArray extends TSingleMap {

		protected $_flags = null;

		public function process ($row) {

			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));

			if ($this->firstrow)
				$this->_flags = array();
				$this->firstrow = false;
			
			$flags = $this->_flags;
			$lookup = $this->lookup;
			
			foreach ($this->fields as $key => $field) {
				$value = $row[$field];
				if (empty($value)) {
					if (($key != $field) && !isset($row[$key]))
						$row[$key] = $value;
				}
				else {
					if (array_key_exists($key, $flags))
						$flag = $flags[$key];
					elseif (is_array($value)) {
						if ((count($value) == 2) && is_numeric($value[0]) && !is_numeric($value[1]))
							$flag = (($key == $field) || is_array($row[$key])) ? true : false;
						else
							$flag = null;
						$this->_flags[$key] = $flags[$key] = $flag;
					}
					else {
						$row[$key] = $lookup[$key][$value];
						continue;
					}
					if ($flag === null) {
						$vals = array();
						foreach ($value as $v)
							$vals[] = $lookup[$key][$v];
						$row[$key] = $vals;
					}
					else {
						if ($flag === true)
							$row[$key][0] = $value[1];
						else
							$row[$key] = $value[1];
					}
				}
			}

			return $row;

		}

	}

	class TDMapArray extends TDoubleMap {

		protected $_flags = null;

		public function process ($row) {

			if (!$this->prepared)
				throw new ETLCodingError(sprintf(self::_NOT_PREP, __METHOD__));

			if ($this->firstrow)
				$this->_flags = array();
				$this->firstrow = false;

			$slookup = $this->slookup;
			$tlookup = $this->tlookup;

			$flags = $this->_flags;

			foreach ($this->fields as $key => $field) {
				$value = $row[$field];
				if (empty($value)) {
					if (($key != $field) && !isset($row[$key]))
						$row[$key] = $value;
				}
				else {
					if (isset($flags[$key]))
						$flag = $flags[$key];
					elseif (is_array($value))
						$this->_flags[$key] = $flags[$key] = $flag = false;
					elseif (strpos($value, ','))
						$this->_flags[$key] = $flags[$key] = $flag = true;
					else {
						$name = $slookup[$key][$value];
						$row[$key] = $tlookup[$key][$name];
						continue;
					}
					$ids = $flag ? explode(',', $value) : $value;
					$value = array();
					foreach ($ids as $id) {
						$name = $slookup[$key][$id];
						$value[] = $tlookup[$key][$name];
					}
					$row[$key] = $flag ? implode(',', $value) : $value;
				}
			}

			return $row;

		}

	}
	
	
	
?>