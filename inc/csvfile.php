<?php


	class CSVException extends Exception {
		protected $last_error = null;
		public function __construct($message, $code = 0) {
			if (is_array($message)) {
				$this->last_error = $message;
				if ($code == 0)
					$code = $message['type'];
				$message = $message['message'];
			}
			parent::__construct($message, $code);
		}
	}


	abstract class CSVFile {

		const DELIMITER = ',';
		const ENCLOSURE = '"';

		public $delimiter = null;
		public $enclosure = null;

		protected $eof = null;
		protected $_fp = null;

		protected function __construct($delimiter=null, $enclosure=null) {
			$this->delimiter = !empty($delimiter) ? $delimiter : self::DELIMITER;
			$this->enclosure = !empty($enclosure) ? $enclosure : self::ENCLOSURE;
		}

		public function __destruct() {
			if ($this->_fp) @fclose($this->_fp);
		}

		public function __get($name) {
			$var = trim($name, '_');
			if (!isset($this->$var))
				throw new CSVException(get_class($this) . ": Property '$name' does not exist.");
			return $this->$var;
		}

		protected function close() {
			if (!$this->_fp)
				throw new CSVException(__METHOD__ . ': CSV file is not open.');
			$r = @fclose($this->_fp);
			if (!$r)
				throw new CSVException(error_get_last());
			$this->eof = null;
			$this->_fp = null;
			return $r;
		}

		protected function open($filename, $mode) {
			if ($this->_fp)
				throw new CSVException(__METHOD__ . ': CSV file is already open.');
			$fp = @fopen($filename, $mode);
			if (!$fp)
				throw new CSVException(error_get_last());
			$this->eof = feof($fp);
			$this->_fp = $fp;
			return $fp;
		}

	}


	class CSVReader extends CSVFile {

		const ESCAPE = '\\';

		public $length = null;
		public $escape = null;

		protected $fields = null;

		public function __construct($delimiter=null, $enclosure=null, $length=0, $escape='\\') {
			parent::__construct($delimiter, $enclosure);
			$this->length = ($length >= 0) ? $length : 0;
			$this->escape = !empty($escape) ? $escape : self::ESCAPE;
		}

		public function close() {
			$this->fields = null;
			return parent::close();
		}

		public function open($filename, $mode = 'r') {
			$r = parent::open($filename, $mode);
			if ($r)
				$this->fields = $this->read();
			return $r;
		}

		public function &read($assoc = false) {
			if (!$this->_fp)
				throw new CSVException(__METHOD__ . ': Tried to read CSV file without first opening it.');
			while ($data = fgetcsv($this->_fp, $this->length, $this->delimiter, $this->enclosure, $this->escape)) {
				if ($data[0] == null)
					continue;
				if ($assoc)
					$data = array_combine($this->fields, $data);
				break;
			}
			if ($data === false)
				$this->eof = feof($this->_fp);
			return $data;
		}

		public function &readcolumn($col, $key = null) {
			$fields = array_flip($this->fields);
			$col = $fields[$col];
			$data = array();
			if ($key !== null) {
				$key = $fields[$key];
				while ($row = $this->read())
					$data[$row[$key]] = $row[$col];
				return $data;
			}
			else {
				while ($row = $this->read())
					$data[] = $row[$col];
				return $data;
			}
		}

	}


	class CSVWriter extends CSVFile {

		protected $header = null;
		protected $header_sent = null;

		public function __construct($delimiter=null, $enclosure=null) {
			parent::__construct($delimiter, $enclosure);
			$this->header_sent = false;
		}

		public function close() {
			$this->header = null;
			$this->header_sent = false;
			return parent::close();
		}

		public function open($filename, $mode = 'w') {
			return parent::open($filename, $mode);
		}

		public function write(&$data, $assoc = false) {
			if ($assoc) {
				if (!$this->header_sent)
					$this->writeheader(array_keys($data));
				return fputcsv($this->_fp, array_values($data), $this->delimiter, $this->enclosure);
			}
			else {
				if (!$this->header_sent)
					throw new CSVException(__METHOD__ . ': CSV Header is not written.');
				return fputcsv($this->_fp, $data, $this->delimiter, $this->enclosure);
			}
		}

		public function writeheader($header) {
			if (empty($header))
				throw new CSVException(__METHOD__ . ': Supplied CSV Header is empty.');
			if ($this->header_sent)
				throw new CSVException(__METHOD__ . ': CSV Header is already written.');
			if (!is_array($header))
				$header = explode(',', $header);
			$r = fputcsv($this->_fp, $header, $this->delimiter, $this->enclosure);
			if (!$r)
				throw new CSVException(__METHOD__ . ': Unable to write CSV Header.');
			$this->header = &$header;
			$this->header_sent = true;
			return $r;
		}

	}



?>