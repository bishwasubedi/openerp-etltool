<?php


	function row_hook($name, $row) {
		if (!is_numeric($row['name']) || !is_numeric($row['product_id']))
			return false;
		return true;
	}
	
	
	require_once(dirname(__FILE__) . '/../lib/etlengine.php');
	
	
	try {
		
		//*
		
		$map = array('product_template', 'product_product');
		
		$runner = new ETLRunner($map);
		
		$engine = $runner->get('product_supplierinfo');
		$engine->hooks->postproc = 'row_hook';
		$result = $engine->run(true, 100);
		
		echo "Result: $result\n";
		
		// */
		
		/*
		$config = array('location'	=> 'http://localhost:4069/xmlrpc/'
						,'model'	=> 'res.partner.address'
						,'database' => 'bi_source'
						,'username' => 'admin'
						,'password' => 'bietl'
						,'fields'	=> 'id,partner_id,title,name,function'
						,'search'	=> "partner_id,=,false+name,!=,''+name,not like,Bazaar"
						,'many2one'	=> 'name'
						);
		$source = new OERPSource(null, $config);
		$source->prepare();
		$data = $source->read(0);
		print_r($data);
		echo 'Total: ' . count($data) . PHP_EOL;
		$source->cleanup();
		#$target = new OERPTarget($engine, $config);
		// */
		
	}
	catch (Exception $e) {
		echo $e;
		exit(1);
	}
	
	exit(0);
	
?>