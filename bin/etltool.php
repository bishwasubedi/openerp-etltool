<?php
	
	require_once(dirname(__FILE__) . '/../lib/etlengine.php');
	
	$write = null;
	$bsize = null;

	$mappings = array();
	
	for ($i = 1; $i < $argc; $i++) {
		$arg = $argv[$i];
		if ($arg == '-w') {
			$write = true;
			continue;
		}
		elseif ($arg == '-b') {
			$bsize = $argv[++$i];
			if (!is_numeric($bsize)) {
				echo "Error! Invalid batchsize: '$bsize'.\n";
				exit(127);
			}
			continue;
		}
		$mappings[] = $arg;
	}

	if (empty($mappings)) {
		echo "Usage: $argv[0] [-w] [-b size] mappings ...\n";
		exit(255);
	}

	try {
		$runner = new ETLRunner($mappings);
		$result = $runner->run($write, $bsize);
		foreach ($result as $k => $v) echo "$k: $v\n";
	}
	catch (ETLError $e) {
		echo $e;
		exit(1);
	}
	
	exit(0);
	
?>