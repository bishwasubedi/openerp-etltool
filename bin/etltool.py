#!/usr/bin/env python

import os
import sys

sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/../lib'))

from etlengine import ETLRunner, ETLError


MIN_SIZE = 0
MAX_SIZE = 1000

ERR_INDEX = "Error! Argument for -b is not given."
ERR_VALUE = "Error! Invalid argument given for -b: %s."
ERR_BSIZE = "Error! Argument for -b must be in between %d and %d."

E_INVALID = "Error! Invalid option: %s."


mappings = []

batchsize = None
writedata = False


i, argc = 1, len(sys.argv)

while i < argc:
    arg = sys.argv[i]
    if arg.startswith('-'):
        if arg == '-w':
            writedata = True
        elif arg == '-b':
            try:
                arg = sys.argv[i+1]
                batchsize = int(arg)
                if batchsize < MIN_SIZE or batchsize > MAX_SIZE:
                    print ERR_BSIZE % (MIN_SIZE, MAX_SIZE)
                    sys.exit(6)
            except IndexError:
                print ERR_INDEX
                sys.exit(5)
            except ValueError:
                print ERR_VALUE % arg
                sys.exit(4)
            i += 1
        else:
            print E_INVALID % arg
            sys.exit(3)
    else:
        mappings.append(arg)
    i += 1

if not mappings:
    print 'Usage: %s [-w] [-b size] mappings...' % sys.argv[0]
    sys.exit(2)

try:
    runner = ETLRunner(mappings)
    result = runner.run(writedata, batchsize)
    for k, v in result.iteritems():
        print '%s: %d' % (k, len(v))
except ETLError as e:
    print e
    sys.exit(1)

sys.exit(0)
